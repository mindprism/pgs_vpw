# Graphite Skin for Podio

##Dark Skin for Podio, with extras

_Skin Podio.com with a dark, Graphite and Gold theme._ 

For [podio.com](http://podio.com)

See main code file: `pgs-1.0/script.js`

Graphite Skin is more than color and styling, it includes enhancements to the Podio UI.

- Many menus and items are now easier to click on
- Element sizes and margins have been reduced for a more compact layout
- Hover indicators now show when items are clickable
- HTML Select elements have been replace with an advanced search widget!
- Pressing F7 will toggle the styling
- Pressing Insert will create a new Item
- More secret keys are available if you make a donation!

#### The Future
Coming very soon will be other versions of the skin similar to Graphite.

- Sandstorm — a version with a light grey palette
- Snowblind — a version with a white palette

#### Graphite Supercharger
Also, keep an eye out for the Graphite Supercharger Chrome Extension for Podio, which will include:

- A full set of Shortcut Keys to enhance your productivity with Podio
- Other widgets and functionality that will improve your Podio experience