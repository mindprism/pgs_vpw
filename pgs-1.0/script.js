/* #INDEX# =============================================================================================================
 * Title .........: Podio Graphite Skin v1.0.0.0
 * AutoIt Version : 3.3
 * Language ......: English (language independent)
 * Description ...: Restyle Podio
 * Author(s) .....: MarkRobbins
 * Copyright .....: Copyright (C) Mark C Robbins. All rights reserved.
 * License .......: Artistic License 2.0, see Artistic.txt
 *
 * Podio Graphite Skin is free software; you can redistribute it and/or modify
 * it under the terms of the Artistic License as published by Larry Wall,
 * either version 2.0, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the Artistic License for more details.
 *
 * You should have received a copy of the Artistic License with this Kit,
 * in the file named "Artistic.txt".  If not, you can get a copy from
 * <http://www.perlfoundation.org/artistic_license_2_0> OR
 * <http://www.opensource.org/licenses/artistic-license-2.0.php>
 *
 * =====================================================================================================================*/

/* #OVERVIEW# ==========================================================================================================
 * SOURCE_______________________________________________________________________________________________________________
  * Organization ..: Mark Robbins and Associates
  * Author ........: Mark Robbins
  * Dependencies ..: <lib> <version> <url>
  *                : visibility.js
  *                : md5.js
  *                : underscore2.js
  *                : tinycolor.js
  *                : jquery-1.11.2.min.js
  *                : color-thief.js
  *                : jdenticon-1.1.0.js
  *                : jquery-ui.js
  *                : lib/ns.js
  *                : lib/path.js
  *                : lib/branch.js
  *                : jqobs.js
  *                : chosen.jquery.js
  *                :
 * LOG__________________________________________________________________________________________________________________
  * Created .......: 2015.11.15.23.03.59
  * Modified ......: 2015.11.15.23.03.59
  * Entries........: yyyy.mm.dd.hh.mm.ss Comments
 * HEADER_______________________________________________________________________________________________________________
  * Type ..........: Script
  * Subtype .......: Main
  * Name ..........: Podio Graphite Skin
  * Summary .......:
  * Description ...: Restyle Podio
  *                  Styling and hotkeys
  * Remarks .......:
  * Features ......:
 * DEVELOPMENT__________________________________________________________________________________________________________
  * Issues ........:
  * Status ........: [ ] New
  *                  [ ] Open
  *                  [ ] InProgress
  *                  [X] Continuing
  *                  [ ] Resolved
  *                  [ ] Closed
 * OTHER________________________________________________________________________________________________________________
  * Related .......:
  * Related Links .:
  * Resources......:
 * =====================================================================================================================*/

// Z_DOT cmd inheritFrom(obj, tgt):void ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.82894262008402301.2015.11.16.07.17.29.828|+!
  function inheritFrom(obj,tgt) {
   var i;
   //console.log('inheritFrom..........'+arguments.callee.caller.name);
   var a=[];
   for (i in obj) {
     if (obj.hasOwnProperty(i)) {
       tgt[i]=obj[i];
       a[a.length]=i;
     }else{
       console.log('not inheriting "'+i+'" in '+obj.name);
     }
   }
   //console.log('inheritedFrom '+obj.name,a);
   }//-inheritFrom

// Z_DOT class typePubSub               ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.80598262008402301.2015.11.16.07.18.09.508|class
  var typePubSub={
    // Z_DOT fn init_pubsub(bug, pfx): ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.67457362008402301.2015.11.16.07.19.35.476|+!
      init_pubsub:function(bug,pfx){
        pfx=pfx===undefined?'__':pfx;
        if (bug){console.log('init_pubsub');}
        if (this._pubs!==undefined) {
          if(bug){console.log('init_pubsub,have pubs');}
          for (i in this._pubs) {
            var f=new Function('o','o.when=new Date();radio("'+i+'").broadcast(o,this);');
            if(bug){console.log('init_pubsub, assigning function '+pfx+i,f);}
            this[pfx+i]=f;
          }
        }
        if (this._subs!==undefined) {
          if(bug){console.log('init_pubsub,have subs');}
          for (i in this._subs) {
            if(bug){console.log('init_pubsub, subscribing to msg:'+i+' with ',this._subs[i]);}
            radio(i).subscribe([this._subs[i],this]);
          }
        }
      }
    // Z_DOT ppb _isPubSub:var         ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.20550462008402301.2015.11.16.07.20.05.502|-,
      ,_isPubSub:true
    };//-typePubSub
// Z_DOT class radio                    ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.78423462008402301.2015.11.16.07.20.32.487|class
  (function(){ // radio
    "use strict";
    //console.log('radio');
      window.radio=radio;
      /**
       * Main Wrapper for radio.$ and create a function radio to accept the channelName
       * @param {String} channelName topic of event
       */
      function radio(channelName) {
        if (arguments.length) {
          radio.$.channel(channelName);
          return radio.$;
        }else{
          return radio.$;
        }
        //arguments.length ? radio.$.channel(channelName) : radio.$.reset();
      }
    radio.$ = {
      version: '0.2',
      channelName: "",
      channels: [],
        /**
         * Reset global state, by removing all channels
         * @example
         *    radio()
         */
        reset: function() {
          radio.$.channelName = "";
          radio.$.channels = [];
          },//-reset
        /**
         * Broadcast (publish)
         * Iterate through all listeners (callbacks) in current channel and pass arguments to subscribers
         * @param arguments data to be sent to listeners
         * @example
         *    //basic usage
         *    radio('channel1').broadcast('my message');
         *    //send an unlimited number of parameters
         *    radio('channel2').broadcast(param1, param2, param3 ... );
         */
        broadcast: function() {
          var i, c = this.channels[this.channelName],
            l = c.length,
            subscriber, callback, context;
          //iterate through current channel and run each subscriber
          for (i = 0; i < l; i++) {
            subscriber = c[i];
            //if subscriber was an array, set the callback and context.
            if ((typeof(subscriber) === 'object') && (subscriber.length)) {
              callback = subscriber[0];
              //if user set the context, set it to the context otherwise, it is a globally scoped function
              context = subscriber[1] || global;
            }
            callback.apply(context, arguments);
          }
          return this;
          },//-broadcast
        /**
         * Create the channel if it doesn't exist and set the current channel/event name
         * @param {String} name the name of the channel
         * @example
         *    radio('channel1');
         */
        channel: function(name) {
          var c = this.channels;
          //create a new channel if it doesn't exists
          if (!c[name]) c[name] = [];
          this.channelName = name;
          return this;
          },//-channel
        channelKeys:function (){
          //console.log('channelKeys');//+m.c.r
          return Object.keys(this.channels);
        },//-channelKeys
        /**
         * Add Subscriber to channel
         * Take the arguments and add it to the this.channels array.
         * @param {Function|Array} arguments list of callbacks or arrays[callback, context] separated by commas
         * @example
         *      //basic usage
         *      var callback = function() {};
         *      radio('channel1').subscribe(callback);
         *
         *      //subscribe an endless amount of callbacks
         *      radio('channel1').subscribe(callback, callback2, callback3 ...);
         *
         *      //adding callbacks with context
         *      radio('channel1').subscribe([callback, context],[callback1, context], callback3);
         *
         *      //subscribe by chaining
         *      radio('channel1').subscribe(callback).radio('channel2').subscribe(callback).subscribe(callback2);
         */
        subscribe: function() {
          var a = arguments,
            c = this.channels[this.channelName],
            i, l = a.length,
            p, ai = [];
          //run through each arguments and subscribe it to the channel
          for (i = 0; i < l; i++) {
            ai = a[i];
            //if the user sent just a function, wrap the fucntion in an array [function]
            p = (typeof(ai) === "function") ? [ai] : ai;
            if ((typeof(p) === 'object') && (p.length)) c.push(p);
          }
          return this;
          },//-subscribe
        /**
         * Remove subscriber from channel
         * Take arguments with functions and unsubscribe it if there is a match against existing subscribers.
         * @param {Function} arguments callbacks separated by commas
         * @example
         *      //basic usage
         *      radio('channel1').unsubscribe(callback);
         *      //you can unsubscribe as many callbacks as you want
         *      radio('channel1').unsubscribe(callback, callback2, callback3 ...);
         *       //removing callbacks with context is the same
         *      radio('channel1').subscribe([callback, context]).unsubscribe(callback);
         */
        unsubscribe: function() {
          var a = arguments,
            i, j, c = this.channels[this.channelName],
            l = a.length,
            cl = c.length,
            offset = 0,
            jo;
          //loop through each argument
          for (i = 0; i < l; i++) {
            //need to reset vars that change as the channel array items are removed
            offset = 0;
            cl = c.length;
            //loop through the channel
            for (j = 0; j < cl; j++) {
              jo = j - offset;
              //if there is a match with the argument and the channel function, unsubscribe it from the channel array
              if (c[jo][0] === a[i]) {
                //unsubscribe matched item from the channel array
                c.splice(jo, 1);
                offset++;
              }
            }
          }
          return this;
          }//-unsubscribe
      };//-radio.%
    return radio;
    }()); //-radio
// Z_DOT ppb __:var                     ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.88316162008402301.2015.11.16.07.16.01.388|+@,
  window.__={
    // Z_DOT var _name:string ::__  ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.25434562008402301.2015.11.16.07.22.23.452|+@,
      _name:'__'
    // Z_DOT struct set::__         ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.95865562008402301.2015.11.16.07.22.36.859|struct
      ,set:{
        // Z_DOT var _name:string ::set::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.04721962008402301.2015.11.16.07.28.32.740|+@,
          ,_name:'set'
        } //-set
    // Z_DOT struct data::__        ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.25444072008402301.2015.11.16.07.30.44.452|struct
      ,data:{
        // Z_DOT var _name:string        ::data::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.61574172008402301.2015.11.16.07.32.27.516|+@,
          _name:'data'
        // Z_DOT struct timers::data::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.42339172008402301.2015.11.16.07.33.13.324|struct
          ,timers:{
            } //-timers
        } //-data
    // Z_DOT struct style::__       ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.69384272008402301.2015.11.16.07.34.08.396|struct
      ,style:{
        // Z_DOT var _name:string                       ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.67419272008402301.2015.11.16.07.34.51.476|+@,
          _name:'style'
        // Z_DOT cmd badgestyle(itemid, ico_url):void   ::style::__  ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.87403372008402301.2015.11.16.07.35.30.478|+!
          ,badgestyle:function(itemid,ico_url){
            var s='';
            s+='#picker .item.badge-item.badgeitem-'+itemid+':before{';
            s+='background-image:url('+ico_url+');';
            s+='}';
            __.utils.updateNamedStyle(s,'badgeitem-'+itemid);
            } //-badgestyle
        // Z_DOT cmd accountstyle(ico_url):void         ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.27798372008402301.2015.11.16.07.36.29.772|+!
          ,accountstyle:function(ico_url){
            var s='';
            s+='#picker .profile:before{';
            s+='background-image:url('+ico_url+');';
            s+='}';
            __.utils.updateNamedStyle(s,'accountstyle');
            } //-accountstyle
        // Z_DOT cmd tabstyle(idx, ico_url):void        ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.35693472008402301.2015.11.16.07.37.19.653|+!
          ,tabstyle:function(idx,ico_url){
            var s='';
            s+='#picker .chosen-drop .chosen-results li.tab.tabidx-'+idx+':before {';
            s+='background-image: url('+ico_url+');';
            s+='}';
            __.utils.updateNamedStyle(s,'tabidx-'+idx);
            } //-tabstyle
        // Z_DOT cmd tabstyle_remove(idx, ico_url):void ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.80136472008402301.2015.11.16.07.37.43.108|+!
          ,tabstyle_remove:function(idx,ico_url){
            if (('#tabidx-'+idx)._one) {
              __.utils.deleteNamedStyle('tabidx-'+idx);
            }
            } //-tabstyle_remove
        // Z_DOT cmd mainstyle_init():void              ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.29610572008402301.2015.11.16.07.38.21.692|+!
          ,mainstyle_init:function(){
            __.utils.addStylesheet('styles.css','graphite');
            } //-mainstyle_init
        // Z_DOT cmd removeMain():void                  ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.13302572008402301.2015.11.16.07.38.40.331|+!
          ,removeMain:function(){
            'body'.q.css('opacity','1').css('visibility','visible');
            '#graphite'.q.remove();
            '#spacenavapp'.q.remove();
            'body'.q.addClass('undone');
            } //-removeMain
        // Z_DOT cmd addMain():void                     ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.25636572008402301.2015.11.16.07.39.23.652|+!
          ,addMain:function(){
            'body'.q.css('opacity','').css('visibility','');
            __.utils.addStylesheet('styles.css','graphite');
            __.utils.addStylesheet('spacenavapp.css','spacenavapp');
            'body'.q.removeClass('undone');
            } //-addMain
        // Z_DOT cmd toggleMain():void                  ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.42997572008402301.2015.11.16.07.39.39.924|+!
            ,toggleMain:function(){
              if ('#graphite'.q.length!==0) {
                this.removeMain();
              }else{
                this.addMain();
              }
              } //-toggleMain
        // Z_DOT fn _spacenavapp_css(x):string          ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.61750672008402301.2015.11.16.07.40.05.716|-?
          ,_spacenavapp_css:function(x){
            var ax=x+2;
            //var pfx='#space-navigation ul.app-nav li:nth-child(12n+1'+ax+')';
            var pfx='#space-navigation ul.app-nav li:nth-child(12n+'+ax+')';
            var rot=x*30;
            rot=rot%360;
            var base='#FCA400';
            var act_box_shadow1=tinycolor(base).darken(35).spin(rot).toString();
            var act_box_shadow2=tinycolor(base).darken(30).spin(rot).toString();
            var bord_hover     =tinycolor(base).darken(20).spin(rot).toString();
            var a_color        =tinycolor(base).darken(10).spin(rot).toString();
            var focus_title    =tinycolor(base).darken( 0).spin(rot).toString();
            var active_title   =tinycolor(base).darken( 0).spin(rot).toString();
            function fixlum(c,h,l){
              h=h?h:0.4;
              l=l?l:0.2;
              var cc=tinycolor(c);
              var ct=0;
              while (true) {
                ct++;
                var lum=cc.getLuminance();
                if (lum>h) {cc=cc.darken(1);}
                if (lum<l) {cc=cc.brighten(1);}
                lum=cc.getLuminance();
                if (lum<h&&lum>l||ct>100) {
                  break;
                }
              }
              return cc.toString();
            }
            a_color=fixlum(a_color,0.3,0.1);
            active_title=fixlum(active_title,0.3,0.1);
            focus_title=fixlum(focus_title,0.3,0.1);
            bord_hover=fixlum(bord_hover,0.3,0.1);
            act_box_shadow1=fixlum(act_box_shadow1,0.3,0.1);
            act_box_shadow2=fixlum(act_box_shadow2,0.3,0.1);
            var s='';
            s+=pfx+'.active {';
            s+="\n";
            s+='  box-shadow: inset 2px 2px 2px 0 '+act_box_shadow1+', inset -2px -2px 7px '+act_box_shadow2+';';
            s+="\n";
            s+='}';
            s+="\n";
            s+=pfx+':focus,';
            s+="\n";
            s+=pfx+':hover {';
            s+="\n";
            s+='  border: 1px solid '+bord_hover+';';
            s+='  transition: border 250ms;';
            s+="\n";
            s+='}';
            s+="\n";
            s+=pfx+' a {';
            s+="\n";
            s+='  color: '+a_color+';';
            s+="\n";
            s+='}';
            s+="\n";
            s+=pfx+'.active .title {';
            s+="\n";
            s+='  color: '+active_title+';';
            s+="\n";
            s+='}';
            s+="\n";
            s+=pfx+' .title,';
            s+="\n";
            s+=pfx+' .title {';
            s+="\n";
            s+='  color: '+a_color+';';
            //s+='lum: '+tinycolor(a_color).getLuminance()+';';
            s+='  transition: color 1s;';
            s+="\n";
            s+='}';
            s+="\n";
            s+=pfx+':focus .title,';
            s+="\n";
            s+=pfx+':hover .title {';
            s+="\n";
            s+='  color: '+focus_title+';';
            s+='  transition: color 250ms;';
            s+="\n";
            s+='}';
            s+="\n";
            s+=pfx+' .icon {';
            s+="\n";
            s+='  -webkit-filter: invert(1) sepia(1) saturate(10) hue-rotate('+rot+'deg);';
            s+="\n";
            s+='}';
            s+="\n";
            return s;
            } //-_spacenavapp
        // Z_DOT cmd spacenavapp_css_wait(s):void       ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.48079672008402301.2015.11.16.07.41.37.084|+!
          ,spacenavapp_css_wait:function(s){
            var that=this;
            if ('head'.q.length===0) {
              setTimeout(function(){
                that.spacenavapp_css_wait(s);
              },2000);
              return;
            }
            __.utils.updateNamedStyle(s,'spacenavapp');
            console.log(s);
            //copyTextToClipboard(s);
            } //-spacenavapp_css_wait
        // Z_DOT cmd spacenavapp_css():void             ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.27102772008402301.2015.11.16.07.42.00.172|+!
          ,spacenavapp_css:function(){
            __.utils.addStylesheet('spacenavapp.css','spacenavapp');
            //__.utils.updateNamedStyle(s,'spacenavapp');
            return;
            var x;
            var s='';
            for (x=0;x<12;x++) {
              s+=this._spacenavapp_css(x);
            }
            this.spacenavapp_css_wait(s);
            //console.log(s);
            //__.utils.updateNamedStyle(s,'spacenavapp');
            } //-spacenavapp_css
        // Z_DOT fn _init_():void                       ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.61524772008402301.2015.11.16.07.42.22.516|-!
          ,_init_:function(){
            var that=this;
            //that.mainstyle_init();
            that.spacenavapp_css();
            } //-_init_
        // Z_DOT cmd _init():void                       ::style::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.08310872008402301.2015.11.16.07.43.21.380|%!
          ,_init:function(){
            this._init_();
            } //-_init
        } //-style
    // Z_DOT struct ctrls::__       ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.05522972008402301.2015.11.16.07.45.22.550|struct
      ,ctrls:{
        // Z_DOT var _name:string ::ctrls::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.40496972008402301.2015.11.16.07.46.09.404|+@,
          _name:'ctrls'
        } //-ctrls
    // Z_DOT struct utils::__       ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.40832082008402301.2015.11.16.07.47.03.804|struct
      ,utils:{
        // Z_DOT var _name:string                                 ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.68227082008402301.2015.11.16.07.47.52.286|+@,
          _name:'utils'
        // Z_DOT fn itemXyWh(q):object{x,y,w,h}                   ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.65197182008402301.2015.11.16.07.49.39.156|+?
          ,itemXyWh:function(q){
            var st=$(window).scrollTop();
            var sl=$(window).scrollLeft();
            var w1=q.width();
            var h1=q.height();
            var x1,y1;
            if (q[0]===window) {
              x1=0;y1=0;
            }else{
              var offset1 = q.offset();
              if (offset1!==undefined) {
                x1=offset1.left-sl;
                y1=offset1.top-st;
              }else{
                //hack!
                x1=sl;y1=st;
              }
            }
            return {x:x1,y:y1,w:w1,h:h1};
            } //-itemXyWh
        // Z_DOT fn cas(e):string                                 ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.67063282008402301.2015.11.16.07.50.36.076|+?
          ,cas:function (e){
            var s = '';
            if (e.ctrlKey) {s += 'c';}
            if (e.altKey) {s += 'a';}
            if (e.shiftKey) {s += 's';}
            return s;
            } //-cas
        // Z_DOT cmd cancel(e):bool                               ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.98397282008402301.2015.11.16.07.51.19.389|+!
          ,cancel:function(e){
            //console.log('keyProcessed');
            e.cancelBubble = true; // IE4+
            try {
              e.keyCode = 0;
            } catch (e) {
            } // IE5
            if (window.event) {e.returnValue = false; } // IE6
            if (e.preventDefault) {e.preventDefault(); } // moz/opera/konqueror
            if (e.stopPropagation) {e.stopPropagation(); } // all
            return false;
            } //-cancel
        // Z_DOT cmd inheritFrom(obj, tgt):void                   ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.16240382008402301.2015.11.16.07.51.44.261|+!
          ,inheritFrom:function(obj,tgt) {
            var i;
            //console.log('inheritFrom..........'+arguments.callee.caller.name);
            var a=[];
            for (i in obj) {
              if (obj.hasOwnProperty(i)) {
                tgt[i]=obj[i];
                a[a.length]=i;
              }else{
                console.log('not inheriting "'+i+'" in '+obj.name);
              }
            }
            //console.log('inheritedFrom '+obj.name,a);
            } //-inheritedFrom
        // Z_DOT cmd eachNode(object, fn, wantTypes, re, _bag):object ::utils::__  ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.23982382008402301.2015.11.16.07.52.08.932|+!
          ,eachNode:function(object,fn,wantTypes,re,_bag){
            /*
             * Iterate object passing its members to fn, filtered by wantTypes and regular expression.
             *
             * Parameters:
             *   object: subject object
             *   fn:     callback - optional
             *     |
             *     signature: name,item,type,object,parent,depth,path,indexes
             *              |
             *              name:      the name of the member
             *              item:      the member
             *              type:      typeof memeber
             *              object:    in which member resides
             *              parent:    the parent of object
             *              depth:     zero based recursion depth
             *              path:      dot path of current object relative to passed object
             *              indexes:   object of indexes
             *                     |
                                   itemIndex:    position item was found in containing object
                                   itemDidIndex: index count of items done for containing object
                                   didIndex:     global index count of items done for passed object
                                   index:        global index count of items for passed object
             *   wantTypes: array of Javascript types, defaults to ['object'] - optional
             *   re:        regular expression to filter member names
             *   _bag:      internal, do not supply, but this it will be returned
             *
             * Returns:
             *   _bag: bag containing collections
             *       |
             *
             *
             *
             *
             */
            var itemIndex= -1, itemDidIndex=-1, name, item, rv,indexes, dupe, retitem;
            if (_bag===undefined){
              var bag={
                  index:-1
                  ,didIndex:-1
                  ,_parents:[]
                  ,_paths:[]
                  ,did:[]
                  ,returns:{items:[],dupes:[]}
              };
              Object.defineProperty(bag,'depth',{
                get:function(){
                  return this._parents.length;
                }
                ,set:function(){}
                ,enumerable:true,configurable: false
              });
              Object.defineProperty(bag,'parent',{
                get:function(){
                  return this._parents.length>0?this._parents[this._parents.length-1]:undefined;
                }
                ,set:function(v){this._parents.push(v);}
                ,enumerable:true,configurable: false
                });
              Object.defineProperty(bag,'parents',{
                get:function(){
                  var a=[];var x;
                  for(x=0;x<this._parents.length;x++){
                    a.push(this._parents[x]);
                  }
                  return a;
                }
                ,set:function(){}
                ,enumerable: true,configurable: false
              });
              Object.defineProperty(bag,'path',{
                get:function(){
                  var p=this._paths.join('.');
                  p=p===''?p:p+'.';
                  return p+''+this._name;
                  }
                ,set:function(){}
                ,enumerable: true,configurable: false
              });
              bag.push=function(){
                this._parents.push(this._object);
                this._paths.push(this._name);
                return __.utils.eachNode(this._item,fn,wantTypes,re,this);
              };
              bag.pop=function(rv){
                this._item=this._parents.pop();
                this._paths.pop();
                this._name=this._paths.length>0?this._paths[this._paths.length-1]:'';
                if (rv===null){this._breaking=true;}
                if (this.depth===0) {
                  return this;
                }
                return rv;
              };
              bag.path=function(){
                var s='';
                var x=0;
                for (x=0;x<this._parents.length;x++){
                  s+='.'+this._parents[x]._name;
                }
                return s;
              }
              _bag=bag;
            } // if no bag
            fn=fn!==undefined?fn:function(name,item,type,object,parent,depth,path,indexes){return true;};
            wantTypes=wantTypes?wantTypes:['object'];
            _bag._object=object;
            for(name in object){
              if(!object.hasOwnProperty(name)){continue;}
              item=object[name];
              _bag._name=name;
              _bag._item=item;
              itemIndex++;
              _bag.index++;
              var type=typeof item;
              if (['function','object'].indexOf(type)!==-1&&item!==null){
                if (_bag.did.indexOf(item)!==-1){
                  indexes={
                    itemIndex:itemIndex
                    ,itemDidIndex:itemDidIndex
                    ,didIndex:_bag.didIndex
                    ,index:_bag.index
                    };
                  dupe={
                    item:item
                    ,name:name
                    ,object:object
                    ,type:type
                    ,parent:_bag.parent
                    ,depth:_bag.depth
                    ,path:_bag.path
                    ,indexes:indexes
                    };
                  _bag.returns.dupes.push(dupe);
                  continue;
                }else{
                  _bag.did.push(item);
                }
              }
              if(wantTypes.indexOf(type)!==-1){
                var doit=re===undefined?true:re.test(name);
                if (doit){
                  itemDidIndex++;
                  _bag.didIndex++;
                  // undefined: no push, true: push, false: no push, -1: no push, 'done':abort level, 'finish': abort all
                  indexes={
                    itemIndex:itemIndex
                    ,itemDidIndex:itemDidIndex
                    ,didIndex:_bag.didIndex
                    ,index:_bag.index
                    };
                  rv=fn(name,item,type,object,_bag.parent,_bag.depth,_bag.path,indexes);
                  if (false){
                  }else if (rv==='done'){
                    break;
                  }else if (rv==='finish'){
                    return _bag.pop(null);
                  }else if (rv){ // '==' is OK here
                    indexes={
                      itemIndex:itemIndex
                      ,itemDidIndex:itemDidIndex
                      ,didIndex:_bag.didIndex
                      ,index:_bag.index
                      };
                    retitem={
                      name:name
                      ,item:item
                      ,object:object
                      ,type:type
                      ,parent:_bag.parent
                      ,depth:_bag.depth
                      ,path:_bag.path
                      ,indexes:indexes
                      };
                    _bag.returns.items.push(retitem);
                  }else if (!rv||rv===-1||rv===undefined){
                    // nada
                  }
                }
              } // not want type
              if (type==='object'&&item!==null){
                rv=_bag.push();
                if(rv===null){
                  return _bag.pop(rv);
                }
              }
              if (_bag._breaking){
                break;
              }
            }
            return _bag.pop();
            } //-eachNode
        // Z_DOT cmd blackHtml():void                             ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.54465482008402301.2015.11.16.07.54.16.445|+!
          ,blackHtml:function(){
            var color = document.documentElement.style.backgroundColor;
            document.documentElement.style.backgroundColor = "black";
            var observer = new MutationObserver(function(mutations) {
              mutations.forEach(function(mutation) {
                if (false&&mutation.target.nodeName == "BODY") {
                  observer.disconnect();
                  document.documentElement.style.backgroundColor = color || "";
                }
              });
            });
            observer.observe(document, { childList: true, subtree: true });
            } //-blackHtml
        // Z_DOT cmd addStylesheet(url, id):void                  ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.21802582008402301.2015.11.16.07.55.20.812|+!
          ,addStylesheet:function(url,id){
            var style = document.createElement('link');
            style.rel = 'stylesheet';
            style.type = 'text/css';
            style.href = chrome.extension.getURL(url);
            if (id) {
              style.id=id;
            }
            (document.head||document.documentElement).appendChild(style);
            } //-addStylesheet
        // Z_DOT cmd addScript(url, id):void                      ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.14714582008402301.2015.11.16.07.55.41.741|+!
          ,addScript:function(url,id){
            var el = document.createElement('script');
            el.type = 'text/javascript';
            el.src = chrome.extension.getURL(url);
            if (id) {
              el.id=id;
            }
            (document.head||document.documentElement).appendChild(el);
            } //-addScript
        // Z_DOT cmd deleteNamedStyle(name_):void                 ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.76395582008402301.2015.11.16.07.55.59.367|+!
          ,deleteNamedStyle:function(name_) {
            __.data.namedStyles=__.data.namedStyles?__.data.namedStyles:[];
            var i=__.data.namedStyles.indexOf(name_);
            if (i!==-1) {
              __.data.namedStyles=__.data.namedStyles.splice(i,1);
              var qo=$('#'+name_);
              if (qo.length==1) {
                qo.remove();
              }else{
                if (qo.length>1) {
                  console.error('multiple element matches for named style:'+name_);
                  qo.remove();
                }else{
                  console.error('zero element matches for named style:'+name_);
                }
              }
            }else{
              console.warn('named style not found:'+name_);
            }
            } //-deleteNamedStyle
        // Z_DOT cmd updateNamedStyle(style_str_, name_, fn):void ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.13487582008402301.2015.11.16.07.56.18.431|+!
          ,updateNamedStyle:function(style_str_,name_,fn) {
            if (name_===undefined) {
              name_='defaultStyle';
            }
            __.data.namedStyles=__.data.namedStyles?__.data.namedStyles:[];
            if (__.data.namedStyles.indexOf(name_)===-1) {
              __.data.namedStyles.push(name_);
            }
            var st=document.getElementById(name_);
            if (st!==null) {
              //st=st[0];
              st.innerHTML=style_str_;
            }else{
              var head = document.getElementsByTagName("HEAD")[0];
              var ele = head.appendChild(window.document.createElement( 'style' ));
              var eleq=$(ele);
              eleq.attr('id',name_);
              if (fn!==undefined) {
                eleq.on('load',fn);
              }
              ele.innerHTML = style_str_;
            }
            } //-updateNamedStyle
        // Z_DOT cmd addScripting(s, id):void                     ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.31254682008402301.2015.11.16.07.57.25.213|+!
          ,addScripting:function(s,id){
            var script = document.createElement("script");
            // Add script content
            script.innerHTML = s;
            if (id!==undefined) {
              script.id=id;
            }
            // Append
            document.head.appendChild(script);
            } //-addScripting
        // Z_DOT cmd addScriptingFn(fn):void                      ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.47146682008402301.2015.11.16.07.57.44.174|+!
          ,addScriptingFn:function(fn){
            var s='';
            s+='('+fn+')();';
            this.addScripting(s);
            } //-addScriptingFn
        // Z_DOT cmd tryClick(q):void                             ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.46748682008402301.2015.11.16.07.58.04.764|+!
          ,tryClick:function(q){
            var ss;
            if (q.hasClass('clickable')) {
              q.simulate('click');
              return;
            }
            if (q.prop('tagName')=='BUTTON') {
              q.click();
              return;
            }
            ss=q.find('>a');
            if (ss.length==1) {
              ss.simulate('click');
              return;
            }
            ss=q.find('>span>span');
            if (ss.length==1) {
              ss.simulate('click');
              return;
            }
            ss=q.find('>span');
            if (ss.length==1) {
              ss.simulate('click');
              return;
            }
            ss=q.find('>div>div');
            if (ss.length==1) {
              ss.simulate('click');
              return;
            }
            if (0) {
              ss=q.find(' div.tooltip');
              if (ss.length>0) {
                ss.eq(0).simulate('click');
                return;
              }
            }
            ss=q.find(' .bd');
            if (ss.length==1) {
              ss.simulate('click');
              return;
            }
            ss=q.find('>div');
            if (ss.length==1) {
              ss.simulate('click');
              return;
            }
            ss=q.find('>div.img');
            if (ss.length==1) {
              ss.simulate('click');
              return;
            }
            q.simulate('click');
            } //-tryClick
        // Z_DOT cmd background_msg(o):void                       ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.33126713008402301.2015.11.16.08.49.22.133|+!
          ,background_msg:function(o){
            chrome.runtime.sendMessage(o, function(response) {});
            } //-background_msg
        // Z_DOT cmd newBackgroundTab(url):void                   ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.09129713008402301.2015.11.16.08.49.52.190|+!
          ,newBackgroundTab:function(url){
            this.background_msg({greeting: "new-background-tab",url:url});
            } //-newBackgroundTab
        // Z_DOT cmd sameBackgroundTab(url):void                  ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.45072813008402301.2015.11.16.08.50.27.054|+!
          ,sameBackgroundTab:function(url){
            this.background_msg({greeting: "same-background-tab",url:url});
            } //-sameBackgroundTab
        // Z_DOT cmd newGoTab(url):void                           ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.51424813008402301.2015.11.16.08.50.42.415|+!
          ,newGoTab:function(url){
            this.background_msg({greeting: "new-go-tab",url:url});
            } //-sameBackgroundTab
        // Z_DOT cmd newOtherBackgroundTab(url):void              ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.06006813008402301.2015.11.16.08.51.00.060|+!
          ,newOtherBackgroundTab:function(url){
            this.background_msg({greeting: "new-obg-tab",url:url});
            } //-sameBackgroundTab
        // Z_DOT cmd newOtherTab(url):void                        ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.35277813008402301.2015.11.16.08.51.17.253|+!
          ,newOtherTab:function(url){
            this.background_msg({greeting: "new-ogo-tab",url:url});
            } //-sameBackgroundTab
        // Z_DOT cmd newNextBackgroundTab(url):void               ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.48619813008402301.2015.11.16.08.51.31.684|+!
          ,newNextBackgroundTab:function(url){
            this.background_msg({greeting: "new-next-bg-tab",url:url});
            } //-sameBackgroundTab
        // Z_DOT cmd newNextTab(url):void                         ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.83821913008402301.2015.11.16.08.51.52.838|+!
          ,newNextTab:function(url){
            this.background_msg({greeting: "new-next-go-tab",url:url});
            } //-sameBackgroundTab
        // Z_DOT cmd activateTab(tabid):void                      ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.94282913008402301.2015.11.16.08.52.08.249|+!
          ,activateTab:function(tabid){
            tabid=parseInt(''+tabid);
            this.background_msg({greeting: "activate-tab",id:tabid});
            } //-activateTab
        // Z_DOT cmd detachTab(tabid):void                        ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.54805913008402301.2015.11.16.08.52.30.845|+!
          ,detachTab:function(tabid){
            tabid=parseInt(''+tabid);
            this.background_msg({greeting: "detach-tab",id:tabid});
            } //-detachTab
        // Z_DOT cmd detachStayTab(tabid):void                    ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.69516913008402301.2015.11.16.08.52.41.596|+!
          ,detachStayTab:function(tabid){
            tabid=parseInt(''+tabid);
            this.background_msg({greeting: "detach-stay-tab",id:tabid});
            } //-detachStayTab
        // Z_DOT cmd attachTab(tabid):void                        ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.65147913008402301.2015.11.16.08.52.54.156|+!
          ,attachTab:function(tabid){
            tabid=parseInt(''+tabid);
            this.background_msg({greeting: "attach-tab",id:tabid});
            } //-attachTab
        // Z_DOT cmd attachStayTab(tabid):void                    ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.03238913008402301.2015.11.16.08.53.03.230|+!
          ,attachStayTab:function(tabid){
            tabid=parseInt(''+tabid);
            this.background_msg({greeting: "attach-stay-tab",id:tabid});
            } //-attachStayTab
        // Z_DOT cmd closeTab(tabid):void                         ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.63449913008402301.2015.11.16.08.53.14.436|+!
          ,closeTab:function(tabid){
            tabid=parseInt(''+tabid);
            this.background_msg({greeting: "close-tab",id:tabid});
            } //-closeTab
        // Z_DOT cmd cloneTab(tabid):void                         ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.86050023008402301.2015.11.16.08.53.25.068|+!
          ,cloneTab:function(tabid){
            tabid=parseInt(''+tabid);
            this.background_msg({greeting: "clone-tab",id:tabid});
            } //-cloneTab
        // Z_DOT cmd cloneStayTab(tabid):void                     ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.94971023008402301.2015.11.16.08.53.37.949|+!
          ,cloneStayTab:function(tabid){
            tabid=parseInt(''+tabid);
            this.background_msg({greeting: "clone-stay-tab",id:tabid});
            } //-cloneTab
        // Z_DOT fn qOf(q):q                                      ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.46304023008402301.2015.11.16.08.54.00.364|+?
          ,qOf:function(q){
            if (false) {
            }else if (typeof q==='function') {return q();
            }else if (q instanceof jQuery) {return q;
            }else if (typeof q==='string') {return q.q;
            }else if (typeof ctrl.q==='object') {
            }
            } //-q
        // Z_DOT fn isInDocument(el):bool                         ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.07896023008402301.2015.11.16.08.54.29.870|+?
          ,isInDocument:function(el) {
            var html = document.body.parentNode;
            while (el) {
              if (el === html) {
                return true;
              }
              el = el.parentNode;
            }
            return false;
            } //-isInDocument
        // Z_DOT cmd body_msg(s):void                             ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.61131123008402301.2015.11.16.08.55.13.116|+!
          ,body_msg:function(s){
            $('body').attr('data-msg',s);
            } //
        // Z_DOT fn timeDifference(earlierDate, laterDate):object{days,hours,mins,seconds} ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.08972123008402301.2015.11.16.08.55.27.980|+?
          ,timeDifference:function(earlierDate,laterDate){
            var tot = laterDate.getTime() - earlierDate.getTime();
            var o = new Object();
            o.days = Math.floor(tot/1000/60/60/24);
            tot -= o.days*1000*60*60*24;
            o.hours = Math.floor(tot/1000/60/60);
            tot -= o.hours*1000*60*60;
            o.minutes = Math.floor(tot/1000/60);
            tot -= o.minutes*1000*60;
            o.seconds = Math.floor(tot/1000);
            return o;
            }
        // Z_DOT cmd requestTabPic():void                         ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.65559123008402301.2015.11.16.08.56.35.556|+!
          ,requestTabPic:function(){
            this.background_msg({greeting: "get-tab-pic"});
            } //-requestTabPic
        // Z_DOT fn matchVizColor(s):bool                         ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.52790223008402301.2015.11.16.08.56.49.725|+?
          ,matchVizColor:function(s){
            function comp(s){
              var co=tinycolor(s);
              var hsl=co.toHsl();
              var c=1;
              var a=0.5;
              var b=0.5;
              var v=Math.sqrt(a*hsl.h*a*hsl.h + b*hsl.s*b*hsl.s + c*hsl.l*c*hsl.l);
              return v;
            }
            var v0=comp(s);
            var at=10000;//Number.Infinity;
            var found;
            vizcolors.forEach(function(cc){
              var v=comp(cc.c);
              //console.log('v',v);
              var dif=Math.abs(v0-v);
              //console.log('dif',dif);
              if (dif<at) {
                //console.log('found..',cc);
                at=dif;
                found=cc;
              }
            });
            if (found) {
              return found;
            }
            } //-matchVizColor
        // Z_DOT fn getIdenticonImageData(ident):object{png,c0,c1,c2,c3,indent,hash} ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.94734223008402301.2015.11.16.08.57.23.749|+?
          ,getIdenticonImageData:function(ident){
            var sc=$('#identicon-canvas');
            if (sc.length==0) {
              $('body').append('<canvas id="identicon-canvas" width="64" height="64"/>');
              //sc=$('<canvas id="identicon-canvas" width="64" height="64"/>');
              sc=$('#identicon-canvas');
            }
            var hash=CryptoJS.MD5(ident).toString();
            sc.jdenticon(hash);
            var rv=sc[0].toDataURL();
            var colorThief = new ColorThief();
            var clr=colorThief.getPalette(sc[0],4,10);
            var c0=tinycolor({r:clr[0][0],g:clr[0][1],b:clr[0][2]});
            var c1=tinycolor({r:clr[1][0],g:clr[1][1],b:clr[1][2]});
            var c2=tinycolor({r:clr[2][0],g:clr[2][1],b:clr[2][2]});
            var c3=tinycolor({r:clr[3][0],g:clr[3][1],b:clr[3][2]});
            return {png:rv,c0:c0,c1:c1,c2:c2,c3:c3,ident:ident,hash:hash};
          }
        // Z_DOT fn fixLuminocity(c, h, l):color                  ::utils::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.37718223008402301.2015.11.16.08.58.01.773|+?
          ,fixLuminocity:function(c,h,l){
            h=h?h:0.4;
            l=l?l:0.2;
            var cc=tinycolor(c);
            var ct=0;
            while (true) {
              ct++;
              var lum=cc.getLuminance();
              if (lum>h) {cc=cc.darken(1);}
              if (lum<l) {cc=cc.brighten(1);}
              lum=cc.getLuminance();
              if (lum<h&&lum>l||ct>100) {
                break;
              }
            }
            return cc;
            } //-fixLuminocity
        } //-utils
    // Z_DOT struct aux::__         ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.96690323008402301.2015.11.16.08.58.29.669|struct
      ,aux:{
        // Z_DOT var _name:string ::aux::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.14116323008402301.2015.11.16.08.59.21.141|+@,
            _name:'aux'
        // Z_DOT struct messaging::aux::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.67469323008402301.2015.11.16.08.59.56.476|struct
          ,messaging:{
            // Z_DOT var _name:string  ::messaging::aux::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.53759423008402301.2015.11.16.09.01.35.735|+@,
              _name:'messaging'
            // Z_DOT cmd _init_():void ::messaging::aux::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.64803523008402301.2015.11.16.09.02.10.846|-!
              ,_init_:function(){
                var s='';
                s+='('+function(){
                  Object.defineProperty(String.prototype,'m',{
                    get:function(){
                      var ss=''+this;
                      var evt=document.createEvent("CustomEvent");
                      evt.initCustomEvent("MraEvent", true, true, ss);
                      return document.dispatchEvent(evt);
                    }
                    ,set:function() { throw "Cannot set Read Only Property 'm'"; }
                    ,enumerable: true
                    ,configurable: false
                  });
                }+')();';
                //setTimeout()
                __.utils.addScripting(s);
                setTimeout(function(){
                  __.utils.addScriptingFn(__.inits.protos);
                  __.utils.addScriptingFn(__.inits.jquery_addons);
                  __.utils.addScript('underscore2.js','underscore2');
                  __.utils.addScript('tinycolor.js','tinycolor');
                },2000);
                } //-_init
            // Z_DOT fn _init():void   ::messaging::aux::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.88188233008402301.2015.11.16.09.14.48.188|%!
              ,_init:function(){
                this._init_();
                } //-init
          } //-messaging
        } //-aux
    // Z_DOT struct inits::__       ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.04372433008402301.2015.11.16.09.17.07.340|struct
      ,inits:{
        // Z_DOT var _name:string         ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.44425533008402301.2015.11.16.09.19.12.444|+@,
          _name:'inits'
        // Z_DOT var bug:bool             ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.96019533008402301.2015.11.16.09.19.51.069|-,
          ,bug:false
        // Z_DOT struct idle::inits::__               ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.46171633008402301.2015.11.16.09.20.17.164|struct
          ,idle:{
            // Z_DOT var _name:string             ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.35250144008402301.2015.11.16.12.15.05.253|+@,
              _name:'idle'
            // Z_DOT cmd styles():void            ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.31245144008402301.2015.11.16.12.15.54.213|-!
              ,styles:function(){
                if (arguments.callee.initialized) {console.error('already initialized');return;}arguments.callee.initialized=true;
                __.style._init();
                } //-styles
            // Z_DOT cmd  messaging():void        ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.09781244008402301.2015.11.16.12.16.58.790|-!
              ,messaging:function(){
                __.aux.messaging._init();
                } //-messaging
            // Z_DOT cmd vis(fn):void             ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.90704244008402301.2015.11.16.12.17.20.709|-!
              ,vis:function(fn){
                //__.aux.vis._init(fn);
                fn&&fn();
                } //-vis
            // Z_DOT cmd menulink():void          ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.37126244008402301.2015.11.16.12.17.42.173|-!
              ,menulink:function(){
                __.ui.menulink._init();
                } //-menulink
            // Z_DOT cmd sort_display():void      ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.75377244008402301.2015.11.16.12.17.57.357|-!
              ,sort_display:function(){
                //__.ui.sort_display._init();
                } //-sort_display
            // Z_DOT cmd picker():void            ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.79319244008402301.2015.11.16.12.18.11.397|-!
              ,picker:function(){
                //__.ui.picker._init();
                } //-picker
            // Z_DOT cmd fixes():void             ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.16030344008402301.2015.11.16.12.18.23.061|-!
              ,fixes:function(){
                __.fixes._init();
                } //-fixes
            // Z_DOT cmd filters_dlg():void       ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.45061344008402301.2015.11.16.12.18.36.054|-!
              ,filters_dlg:function(){
                //__.ui.filters_dlg._init();
                } //-filters_dlg
            // Z_DOT cmd mo_filters_dlg():void    ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.18783344008402301.2015.11.16.12.18.58.781|-!
                ,mo_filters_dlg:function(){
                  //__.mo.filters_dlg._init();
                  } //-mo_filters_dlg
            // Z_DOT evt _on_vis_complete_():void ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.26996344008402301.2015.11.16.12.19.29.962|-\
              ,_on_vis_complete_:function(){
                var that=__.inits.idle;
                that.sort_display();
                that.picker();
                that.fixes();
                that.filters_dlg();
                that.mo_filters_dlg();
                __.ui.body.present();
                } //-_on_vis_complete_
            // Z_DOT cmd _init_():void            ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.52311444008402301.2015.11.16.12.20.11.325|-!
              ,_init_:function(){
                __.utils.body_msg('idle inits...');
                var that=this;
                this.styles();
                this.messaging();
                this.menulink();
                __.utils.body_msg('idle inits done');
                __.inits.idle.vis(this._on_vis_complete_);
                } //-_init_
            // Z_DOT cmd _init():void             ::idle::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.34047444008402301.2015.11.16.12.21.14.043|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-idle
        // Z_DOT cmd protos():void        ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.41432644008402301.2015.11.16.12.23.43.414|-!
          ,protos:function(){
            this.bug&&console.log('bugging:'+arguments.callee.name);
            if (arguments.callee.initialized) {console.error('already initialized');return;}arguments.callee.initialized=true;
            var __entityMap = {
              "&": "&amp;",
              "<": "&lt;",
              ">": "&gt;",
              '"': '&quot;',
              "'": '&#39;',
              "/": '&#x2F;'
              };//-__entityMap
            String.prototype.escapeHTML = function() {
              return String(this).replace(/[&<>"'\/]/g, function (s) {
                  return __entityMap[s];
              });
              }; //-escapeHTML
            String.prototype.qo=function(){
              var s=''+this;
              return $(s);
              }; //-qo
            String.prototype._ropf=function(o,fn){
              var n=''+this;
              Object.defineProperty(o,n, {
                get: fn
                ,set: function() { throw "Cannot set Read Only Property '"+n+"'"; }
                ,enumerable: true
                ,configurable: false
              });
              }; //-_ropf
            String.prototype._ropq=function(o,sel){
              var n=''+this;
              Object.defineProperty(o, n, {
                get: function(){
                  return $(sel);
                }
                ,set: function() { throw "Cannot set Read Only Property '"+n+"'"; }
                ,enumerable: true
                ,configurable: false
              });
              }; //-_ropq
            String.prototype._ropqc=function(o,sel){
              var n=''+this;
              Object.defineProperty(o, n, {
                get: function(){
                  if (!arguments.callee.cache) {
                    arguments.callee.cache=$(sel);
                  }
                  return arguments.callee.cache;
                }
                ,set: function() { throw "Cannot set Read Only Property '"+n+"'"; }
                ,enumerable: true
                ,configurable: false
              });
              }; //-_ropqc
            String.prototype._pf=function(o,get_f,set_f){
              var n=''+this;
              Object.defineProperty(o, n, {
                get: get_f
                ,set: set_f
                ,enumerable: true
                ,configurable: false
              });
              }; //-_pf
            's'._ropf(String.prototype,function(){
              console.log(''+this,this.q[0]);
              return this.q;
              }); // -q
            'q'._ropf(String.prototype,function(){
              return this.qo();
              }); // -q
            'e'._ropf(String.prototype,function(){
              var q=this.qo();
              console.info(this+':',q.length);
              var dp=q.getDomPath();
              console.info(dp);
              var t='2px dotted red';
              location.href='aip://clip/'+escape(dp);
              return q
               .css('border',t)
               .css('border-left',t)
               .css('border-top',t)
               .css('border-bottom',t)
               .css('border-right',t)
               .css('box-shadow','inset 0px 0px 10px 2px #F00')
               .css('-webkit-filter','brightness(5) contrast(1.6)');
            });
            'ee'._ropf(String.prototype,function(){
              var q=this.qo();
              console.info(this+':',q.length);
              return q
               .css('border','')
               .css('border-left','')
               .css('border-top','')
               .css('border-bottom','')
               .css('border-right','')
               .css('box-shadow','')
               .css('-webkit-filter','');
            });
            'exists'._ropf(String.prototype,function(){
              var s=''+this;
              return s.q.length!==0;
            });
            '_one'._ropf(String.prototype,function(){
              var s=''+this;
              return s.q.length===1;
            });
            'p'._ropf(String.prototype,function(){
              var s=''+this;
              if (s.exists) {
                if (s.q.length==1) {
                  return s.q.getDomPath();
                }
                var a=[];
                s.q.each(function(){
                  var me=$(this);
                  a.push(me.getDomPath());
                });
                return a;
              }
              return null;
            });
            'singleSpace'._ropf(String.prototype,function(){
              var s=''+this;
              return s.replace(/\s{2,}/g, ' ');
            });
            '_trim'._ropf(String.prototype,function(){
              var s=''+this;
              return $.trim(s);
            });
            '_abbr'._ropf(String.prototype,function(){
               var s=''+this;
               if (s==='') {
                 return '';
               }
               s=s.replace(/[^a-zA-Z0-9]/g,' ');
               var a=[];
               var x;
               var p_type;
               var m={su:true,sl:true,sn:true,nl:true,nu:true,lu:true,uu:true,ln:true,un:true};
               for(x=0;x<s.length;x++){
                 var e=s.substr(x,1);
                 var is_type;//ulns
                 if ('0123456789'.indexOf(e)>=0){is_type='n';
                 }else if('abcdefghijklmnopqrstuvwxyz'.indexOf(e)>=0){is_type='l';
                 }else if('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.indexOf(e)>=0){is_type='u';
                 }else{is_type='s';
                 }
                 if(p_type===undefined){
                   if(is_type!=='s'){
                     a.push(e);
                   }
                 }else{
                   if (m[p_type+is_type]){
                     a.push(e);
                   }
                 }
                 p_type=is_type;
               }
               return a.join('').toUpperCase();
            });
            '_numwrap'._ropf(String.prototype,function(){
               var s=''+this;
               return '&#9129;'+s+'&#9131;';
            });
            '_f4'._ropf(String.prototype,function(){
               var s=''+this;
               setTimeout(function(){
                 s.q.focus();
               },4000);
               return s.q[0];
            });
            '_viz'._ropf(String.prototype,function(){
               var s=''+this;
               function comp(s){
                 var co=tinycolor(s);
                 var hsl=co.toHsl();
                 var c=1;
                 var a=0.5;
                 var b=0.5;
                 var v=Math.sqrt(a*hsl.h*a*hsl.h + b*hsl.s*b*hsl.s + c*hsl.l*c*hsl.l);
                 return v;
               }
               var v0=comp(s);
               var at=10000;//Number.Infinity;
               var found;
               vizcolors.forEach(function(cc){
                 var v=comp(cc.c);
                 //console.log('v',v);
                 var dif=Math.abs(v0-v);
                 //console.log('dif',dif);
                 if (dif<at) {
                   //console.log('found..',cc);
                   at=dif;
                   found=cc;
                 }
               });
               if (found) {
                 return found;
               }
            });
            //'.simple-balloon.app-filters-popover .navigation li.selected'._f4
            String.prototype._abbr2=function(allows){
              var s=''+this;
              if (s==='') {
                return '';
              }
              var allows2=allows.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
              var re=new RegExp('[^a-zA-Z0-9'+allows2+']','g');
              s=s.replace(re,' ');
              var s0=s.replace(/ /g,'');
              var a=[];
              var x;
              var p_type;
              var m={su:true,sl:true,sn:true,nl:true,nu:true,lu:true,uu:true,ln:true,un:true};
              for(x=0;x<s.length;x++){
                var e=s.substr(x,1);
                var is_type;//ulns
                if ('0123456789'.indexOf(e)>=0){is_type='n';
                }else if('abcdefghijklmnopqrstuvwxyz'.indexOf(e)>=0){is_type='l';
                }else if('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.indexOf(e)>=0){is_type='u';
                }else{is_type='s';
                }
                if(p_type===undefined){
                  if(is_type!=='s'){
                    a.push(e);
                  }else{
                    if (allows.indexOf(e)>=0) {a.push(e);}
                  }
                }else{
                  if (m[p_type+is_type]){
                    a.push(e);
                  }else{
                    if (allows.indexOf(e)>=0) {a.push(e);}
                  }
                }
                p_type=is_type;
              }
              if (a.length==1) {
                a.push(s0.substr(1,2));
              }
              return a.join('').toUpperCase();
            }
            String.prototype._bc=function(o,that_){return radio(this.toString()).broadcast(o,that_);}
            String.prototype._sub=function(a1,a2,a3,a4){return radio(this.toString()).subscribe(a1,a2,a3,a4);}
            } //-protos
        // Z_DOT cmd jquery_addons():void ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.43059644008402301.2015.11.16.12.24.55.034|-!
          ,jquery_addons:function(){
            if (arguments.callee.initialized) {console.error('already initialized');return;}arguments.callee.initialized=true;
            //noinspection JSUnresolvedVariable
            (function($){
              $.fn.replaceText = function( search, replace, text_only ) {
                return this.each(function(){
                  var node = this.firstChild,val,new_val,remove = [];
                  if ( node ) {
                    do {
                      if ( node.nodeType === 3 ) {
                        val = node.nodeValue;new_val = val.replace( search, replace );
                        if ( new_val !== val ) {
                          if ( !text_only && /</.test( new_val ) ) {
                            $(node).before( new_val );remove.push( node );
                          } else {
                            node.nodeValue = new_val;
                          }
                        }
                      }
                    } while ( node = node.nextSibling );
                  }
                  remove.length && $(remove).remove();
                });
              };
            })(jQuery);
            //noinspection JSUnresolvedVariable
            (function($){
              $.fn.visibles = function() {
                return this.filter(function(){
                  if (this.isVisible()) {
                    return false;
                  }
                  return true;
                });
              };
            })(jQuery);
            //noinspection JSUnresolvedVariable
            (function($){
              $.fn.withText = function( search ) { //+ text_only second arg unused
                var rv=this.contents().filter(
                  function(){
                    if (this.nodeType!==3) {
                      return false;
                    }
                    //var me=$(this);
                    var txt=this.nodeValue;
                    if (typeof search=='object') {//re
                      return search.test(txt);
                    }
                    return txt.indexOf(search)!=-1;
                  }
                );
                return rv.parent();
             };
            })(jQuery);
            //noinspection JSUnresolvedVariable
            (function( $ ){
              var getStringForElement = function (el) {
                var string = el.tagName.toLowerCase();
                if (el.id) {
                  string += "#" + el.id;
                }
                if (el.className) {
                  if (typeof el.className==='string') {
                    if (el.className!='') {
                      string += "." + el.className.replace(/  */g,' ').replace(/ /g, '.');
                    }
                  }
                }
                string=string.replace(/\.\.*/,'.');
                return string;
              };
              $.fn.getDomPath = function(string) {
                if (typeof(string) == "undefined") {
                  string = true;
                }
                var p = [],el = $(this).first();
                el.parents().not('html').each(function() {
                  p.push(getStringForElement(this));
                });
                p.reverse();
                p.push(getStringForElement(el[0]));
                return string ? p.join(" > ") : p;
              };
            })( jQuery );
            ;(function( $, undefined ) {
            var rkeyEvent = /^key/,
              rmouseEvent = /^(?:mouse|contextmenu)|click/;
            $.fn.simulate = function( type, options ) {
              return this.each(function() {
                new $.simulate( this, type, options );
              });
            };
            $.simulate = function( elem, type, options ) {
              var method = $.camelCase( "simulate-" + type );
              this.target = elem;
              this.options = options;
              if ( this[ method ] ) {
                this[ method ]();
              } else {
                this.simulateEvent( elem, type, options );
              }
            };
            $.extend( $.simulate, {
              keyCode: {
                BACKSPACE: 8,
                COMMA: 188,
                DELETE: 46,
                DOWN: 40,
                END: 35,
                ENTER: 13,
                ESCAPE: 27,
                HOME: 36,
                LEFT: 37,
                NUMPAD_ADD: 107,
                NUMPAD_DECIMAL: 110,
                NUMPAD_DIVIDE: 111,
                NUMPAD_ENTER: 108,
                NUMPAD_MULTIPLY: 106,
                NUMPAD_SUBTRACT: 109,
                PAGE_DOWN: 34,
                PAGE_UP: 33,
                PERIOD: 190,
                RIGHT: 39,
                SPACE: 32,
                TAB: 9,
                UP: 38
              },
              buttonCode: {
                LEFT: 0,
                MIDDLE: 1,
                RIGHT: 2
              }
            });
            $.extend( $.simulate.prototype, {
              simulateEvent: function( elem, type, options ) {
                var event = this.createEvent( type, options );
                this.dispatchEvent( elem, type, event, options );
              },
              createEvent: function( type, options ) {
                if ( rkeyEvent.test( type ) ) {
                  return this.keyEvent( type, options );
                }
                if ( rmouseEvent.test( type ) ) {
                  return this.mouseEvent( type, options );
                }
              },
              mouseEvent: function( type, options ) {
                var event, eventDoc, doc, body;
                options = $.extend({
                  bubbles: true,
                  cancelable: (type !== "mousemove"),
                  view: window,
                  detail: 0,
                  screenX: 0,
                  screenY: 0,
                  clientX: 1,
                  clientY: 1,
                  ctrlKey: false,
                  altKey: false,
                  shiftKey: false,
                  metaKey: false,
                  button: 0,
                  relatedTarget: undefined
                }, options );
                if ( document.createEvent ) {
                  event = document.createEvent( "MouseEvents" );
                  event.initMouseEvent( type, options.bubbles, options.cancelable,
                    options.view, options.detail,
                    options.screenX, options.screenY, options.clientX, options.clientY,
                    options.ctrlKey, options.altKey, options.shiftKey, options.metaKey,
                    options.button, options.relatedTarget || document.body.parentNode );
                  // IE 9+ creates events with pageX and pageY set to 0.
                  // Trying to modify the properties throws an error,
                  // so we define getters to return the correct values.
                  if ( event.pageX === 0 && event.pageY === 0 && Object.defineProperty ) {
                    eventDoc = event.relatedTarget.ownerDocument || document;
                    doc = eventDoc.documentElement;
                    body = eventDoc.body;
                    Object.defineProperty( event, "pageX", {
                      get: function() {
                        return options.clientX +
                          ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) -
                          ( doc && doc.clientLeft || body && body.clientLeft || 0 );
                      }
                    });
                    Object.defineProperty( event, "pageY", {
                      get: function() {
                        return options.clientY +
                          ( doc && doc.scrollTop || body && body.scrollTop || 0 ) -
                          ( doc && doc.clientTop || body && body.clientTop || 0 );
                      }
                    });
                  }
                } else if ( document.createEventObject ) {
                  event = document.createEventObject();
                  $.extend( event, options );
                  // standards event.button uses constants defined here: http://msdn.microsoft.com/en-us/library/ie/ff974877(v=vs.85).aspx
                  // old IE event.button uses constants defined here: http://msdn.microsoft.com/en-us/library/ie/ms533544(v=vs.85).aspx
                  // so we actually need to map the standard back to oldIE
                  event.button = {
                    0: 1,
                    1: 4,
                    2: 2
                  }[ event.button ] || ( event.button === -1 ? 0 : event.button );
                }
                return event;
              },
              keyEvent: function( type, options ) {
                var event;
                options = $.extend({
                  bubbles: true,
                  cancelable: true,
                  view: window,
                  ctrlKey: false,
                  altKey: false,
                  shiftKey: false,
                  metaKey: false,
                  keyCode: 0,
                  charCode: undefined
                }, options );
                if ( document.createEvent ) {
                  try {
                    event = document.createEvent( "KeyEvents" );
                    event.initKeyEvent( type, options.bubbles, options.cancelable, options.view,
                      options.ctrlKey, options.altKey, options.shiftKey, options.metaKey,
                      options.keyCode, options.charCode );
                  // initKeyEvent throws an exception in WebKit
                  // see: http://stackoverflow.com/questions/6406784/initkeyevent-keypress-only-works-in-firefox-need-a-cross-browser-solution
                  // and also https://bugs.webkit.org/show_bug.cgi?id=13368
                  // fall back to a generic event until we decide to implement initKeyboardEvent
                  } catch( err ) {
                    event = document.createEvent( "Events" );
                    event.initEvent( type, options.bubbles, options.cancelable );
                    $.extend( event, {
                      view: options.view,
                      ctrlKey: options.ctrlKey,
                      altKey: options.altKey,
                      shiftKey: options.shiftKey,
                      metaKey: options.metaKey,
                      keyCode: options.keyCode,
                      charCode: options.charCode
                    });
                  }
                } else if ( document.createEventObject ) {
                  event = document.createEventObject();
                  $.extend( event, options );
                }
                if ( !!/msie [\w.]+/.exec( navigator.userAgent.toLowerCase() ) || (({}).toString.call( window.opera ) === "[object Opera]") ) {
                  event.keyCode = (options.charCode > 0) ? options.charCode : options.keyCode;
                  event.charCode = undefined;
                }
                return event;
              },
              dispatchEvent: function( elem, type, event ) {
                if ( elem[ type ] ) {
                  elem[ type ]();
                } else if ( elem.dispatchEvent ) {
                  elem.dispatchEvent( event );
                } else if ( elem.fireEvent ) {
                  elem.fireEvent( "on" + type, event );
                }
              },
              simulateFocus: function() {
                var focusinEvent,
                  triggered = false,
                  element = $( this.target );
                function trigger() {
                  triggered = true;
                }
                element.bind( "focus", trigger );
                element[ 0 ].focus();
                if ( !triggered ) {
                  focusinEvent = $.Event( "focusin" );
                  focusinEvent.preventDefault();
                  element.trigger( focusinEvent );
                  element.triggerHandler( "focus" );
                }
                element.unbind( "focus", trigger );
              },
              simulateBlur: function() {
                var focusoutEvent,
                  triggered = false,
                  element = $( this.target );
                function trigger() {
                  triggered = true;
                }
                element.bind( "blur", trigger );
                element[ 0 ].blur();
                // blur events are async in IE
                setTimeout(function() {
                  // IE won't let the blur occur if the window is inactive
                  if ( element[ 0 ].ownerDocument.activeElement === element[ 0 ] ) {
                    element[ 0 ].ownerDocument.body.focus();
                  }
                  // Firefox won't trigger events if the window is inactive
                  // IE doesn't trigger events if we had to manually focus the body
                  if ( !triggered ) {
                    focusoutEvent = $.Event( "focusout" );
                    focusoutEvent.preventDefault();
                    element.trigger( focusoutEvent );
                    element.triggerHandler( "blur" );
                  }
                  element.unbind( "blur", trigger );
                }, 1 );
              }
            });
            /** complex events **/
            function findCenter( elem ) {
              var offset,
                document = $( elem.ownerDocument );
              elem = $( elem );
              offset = elem.offset();
              return {
                x: offset.left + elem.outerWidth() / 2 - document.scrollLeft(),
                y: offset.top + elem.outerHeight() / 2 - document.scrollTop()
              };
            }
            function findCorner( elem ) {
              var offset,
                document = $( elem.ownerDocument );
              elem = $( elem );
              offset = elem.offset();
              return {
                x: offset.left - document.scrollLeft(),
                y: offset.top - document.scrollTop()
              };
            }
            $.extend( $.simulate.prototype, {
              simulateDrag: function() {
                var i = 0,
                  target = this.target,
                  eventDoc = target.ownerDocument,
                  options = this.options,
                  center = options.handle === "corner" ? findCorner( target ) : findCenter( target ),
                  x = Math.floor( center.x ),
                  y = Math.floor( center.y ),
                  coord = { clientX: x, clientY: y },
                  dx = options.dx || ( options.x !== undefined ? options.x - x : 0 ),
                  dy = options.dy || ( options.y !== undefined ? options.y - y : 0 ),
                  moves = options.moves || 3;
                this.simulateEvent( target, "mousedown", coord );
                for ( ; i < moves ; i++ ) {
                  x += dx / moves;
                  y += dy / moves;
                  coord = {
                    clientX: Math.round( x ),
                    clientY: Math.round( y )
                  };
                  this.simulateEvent( eventDoc, "mousemove", coord );
                }
                if ( $.contains( eventDoc, target ) ) {
                  this.simulateEvent( target, "mouseup", coord );
                  this.simulateEvent( target, "click", coord );
                } else {
                  this.simulateEvent( eventDoc, "mouseup", coord );
                }
              }
            });
            })( jQuery );
            $.fn.extend({
              hasClasses: function (selectors) {
                var self = this;
                for (var i in selectors) {
                  if ($(self).hasClass(selectors[i])) {
                    return true;
                  }
                }
                return false;
              } // fn
            });
            } //-jquery_addons
        // Z_DOT cmd ear():void           ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.57597744008402301.2015.11.16.12.26.19.575|-!
          ,ear:function(){
            __.ear._init();
            } //-ear
        // Z_DOT cmd key_hook():void      ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.34822844008402301.2015.11.16.12.27.02.843|-!
          ,key_hook:function(){
            if (arguments.callee.initialized) {console.error('already initialized');return;}arguments.callee.initialized=true;
            // KEY backspace delete
            //
            $(document).keypress(function(evt){
              return __.event.doc.keypress(evt);
            });
            $(document).keydown(function(evt){
              return __.event.doc.keydown(evt);
            });
            $(document).keyup(function(evt){
              return __.event.doc.keyup(evt);
            });
            } //-key_hook
        // Z_DOT cmd mouse_hook():void    ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.49720944008402301.2015.11.16.12.28.22.794|-!
          ,mouse_hook:function(){
            if (arguments.callee.initialized) {console.error('already initialized');return;}arguments.callee.initialized=true;
            $(document).mousemove(function(evt){
              __.data.mmx=evt.pageX;
              __.data.mmy=evt.pageY;
              __.event.doc.mousemove(evt);
              //console.info('evt.pageX:',evt.pageX,'evt.pageY',evt.pageY);
            });
            return;
            function setup_hook(tgt,evt_name,map,filt,alt_name){
              alt_name=alt_name?alt_name:evt_name;
              filt=filt?filt:'body';
              $(tgt).on(evt_name,filt,null,function(evt){//works on btn 3
                var me=$(evt.target);
                var path=me.getDomPath();
                if (_.ctrls.is_cursor_tool(me)) {
                  _.watch.cursor_tool.check();
                }
                if (path.indexOf('.app-button')!==-1) {
                  //+ fix and hookup
                  //return;
                }
                //
                console.log(evt_name+':which '+evt.which);
                console.log(evt_name+':path '+path);
                console.log(evt_name+':evt.target.tagName '+evt.target.tagName);
                var cas=_.utils.cas(evt);
                //var map={
                //  bg:{p:'> div.underlay-inner > svg',r:_.event.doc.bg}
                //  ,move_cross:{p:'> div.cross-overlay.cur_sel__0',r:_.event.move_cross}
                //};
                var dest;
                var root;
                for (var i in map) {
                  if (!map.hasOwnProperty(i)) {
                    continue;
                  }
                  if (path.indexOf(map[i].p)!==-1) {
                    dest=i;
                    root=map[i].r;
                  }
                }
                if (!root) {
                  console.log('no root found for path:'+path);
                }else{
                  console.log(Object.keys(root),evt_name+''+cas+evt.which);
                }
                if (dest) {
                  if (typeof root[alt_name+''+cas+evt.which]==='function') {
                    return root[alt_name+''+cas+evt.which](evt);
                  }else{
                    console.log('root.'+alt_name+''+cas+evt.which+' is not a function for dest:'+dest);
                  }
                }
                //
              });
            }
            if (0) {
              //+ fix and hookup
              setup_hook(document,'click',{
                bg:{p:'> div.underlay-inner > svg',r:_.event.doc.bg}
              });
              setup_hook(document,'dblclick',{
                bg:{p:'> div.underlay-inner > svg',r:_.event.doc.bg}
                ,move_cross:{p:'> div.cross-overlay.cur_sel__0',r:_.event.move_cross}
              });
              setup_hook('div.underlay-inner','mouseup',{
                bg:{p:'> div.underlay-inner > svg',r:_.event.doc.bg}
              },'*','click');
            }
            //
            if (0) {
              //noinspection JSUnresolvedVariable
              'div.underlay-inner'.q.on('mouseup','*',null,function(evt){
                var evt_name='mouseup';
                var path=$(evt.target).getDomPath();
                console.log('mouseup '+evt.which);
                console.log(evt_name+':path '+path);
                if (evt.which==2) {
                  console.log('mouse 2');
                  return true;
                }
              });
              //noinspection JSUnresolvedVariable
              'div.underlay-inner'.q.on('click','*',null,function(evt){
                var evt_name='click';
                var path=$(evt.target).getDomPath();
                console.log(evt_name+' '+evt.which);
                console.log(evt_name+':path '+path);
                if (evt.which==2) {
                  console.log('mouse 2');
                  return true;
                }
              });
              //noinspection JSUnresolvedVariable
              'div.underlay-inner'.q.on('mouseup','*',null,function(evt){
                var evt_name='mouseup';
                var path=$(evt.target).getDomPath();
                console.log('mouseup '+evt.which);
                console.log(evt_name+':path '+path);
                if (evt.which==2) {
                  console.log('mouse 2');
                  return true;
                }
              });
              //noinspection JSUnresolvedVariable
              'div.underlay-inner'.q.on('mousedown','*',null,function(evt){//works on btn 3
                var evt_name='mousedown';
                var path=$(evt.target).getDomPath();
                console.log('mousedown '+evt.which);
                console.log(evt_name+':path '+path);
                if (evt.which==2) {
                  console.log('mouse 2');
                  return true;
                }
              });
            }
            //
            if (0) {
              $(document).on('mouseup','body',null,function(evt){
                console.log('mouseup '+evt.which);
                if (evt.which==2) {
                  console.log('mouse 2');
                  return true;
                }
              });
              $(document).on('mousedown','body',null,function(evt){//works on btn 3
                console.log('mousedown '+evt.which);
                if (evt.which==2) {
                  console.log('mouse 2');
                  return true;
                }
              });
              $(document).on('click','body',null,function(evt){//works on btn 3
                console.log('click '+evt.which);
                if (evt.which==2) {
                  console.log('mouse 2');
                  return true;
                }
              });
            }
            } //-mouse_hook
        // Z_DOT cmd dom_hook():void      ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.77926944008402301.2015.11.16.12.29.22.977|-!
          ,dom_hook:function(){
            if (arguments.callee.initialized) {console.error('already initialized');return;}arguments.callee.initialized=true;
            $(document).bind('DOMNodeInserted', function(event) {
              __.event.dom.node_inserted(event);
            });
            $(document).bind('DOMNodeRemoved', function(event) {
              __.event.dom.node_removed(event);
            });
            } //-dom_hook
        // Z_DOT cmd hooks():void         ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.91121054008402301.2015.11.16.12.30.12.119|-!
          ,hooks:function(){
            __.hooks._init();
            } //-hooks
        // Z_DOT cmd blackHtml():void     ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.59537054008402301.2015.11.16.12.31.13.595|-!
          ,blackHtml:function(){
            __.utils.blackHtml();
            } //-blackHtml
        // Z_DOT cmd chosen():void        ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.34853154008402301.2015.11.16.12.32.15.843|-!
          ,chosen:function(){
            __.chosen._init();
            } //chosen
        // Z_DOT cmd _init_():void        ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.77758154008402301.2015.11.16.12.33.05.777|-!
          ,_init_:function(){
            __.utils.body_msg('inits...');
            this.blackHtml();
            this.protos();
            this.jquery_addons();
            this.ear();
            this.hooks();
            this.key_hook();
            //__.inits.styles();
            this.chosen();
            __.utils.body_msg('inits done');
            //this.fixes();
            } //-_init_
        // Z_DOT cmd _init():void         ::inits::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.89264254008402301.2015.11.16.12.34.06.298|%!
          ,_init:function(){
            this._init_();
            } //-_init
        } //-inits
    // Z_DOT struct hooks::__       ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.74469254008402301.2015.11.16.12.34.56.447|struct
      ,hooks:{
        // Z_DOT var _name:string       ::hooks::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.00691454008402301.2015.11.16.12.36.59.600|+@,
          _name:'hooks'
        // Z_DOT struct windowScrollHook::hooks::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.32118454008402301.2015.11.16.12.38.01.123|struct
          ,windowScrollHook:{
            // Z_DOT var _name:string     ::windowScrollHook::hooks::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.41949554008402301.2015.11.16.12.39.54.914|+@,
              _name:'windowScrollHook'
            // Z_DOT struct _pubs::windowScrollHook::hooks::__          ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.82646654008402301.2015.11.16.12.41.04.628|struct
              ,_pubs:{
                // Z_DOT msg windowScroll{evt} ::_pubs::windowScrollHook::hooks::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.82972754008402301.2015.11.16.12.42.07.928|+\
                windowScroll:{evt:null}
                } //-_pubs
            // Z_DOT cmd _set_hook():void ::windowScrollHook::hooks::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.39456854008402301.2015.11.16.12.44.25.493|-!
              ,_set_hook:function(){
                var that=this;
                $(window).scroll(function(evt) {
                  that.windowScroll({evt:evt});
                });
                } //-_set_hook
            // Z_DOT cmd _init():void     ::windowScrollHook::hooks::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.38103954008402301.2015.11.16.12.45.30.183|%!
              ,_init:function(){
                inheritFrom(typePubSub,this);
                this.init_pubsub(false,'');
                this._set_hook();
                } //-_init
            } //-windowScrollHook
        // Z_DOT cmd _init_():void      ::hooks::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.73521064008402301.2015.11.16.12.46.52.537|-!
          ,_init_:function(){
            var i;
            for (i in this) {
              if (!this.hasOwnProperty(i)) {
                continue;
              }
              if (false) {
              }else if (typeof this[i]==='function') {
                if (/^.+_init$/.test(i)) {
                  this[i]();
                }
              }else if (typeof this[i]==='object') {
                if (typeof this[i]._init==='function') {
                  this[i]._init();
                }
              }
            }
            } //-_init_
        // Z_DOT cmd _init():void       ::hooks::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.21612364008402301.2015.11.16.12.52.01.612|%!
          ,_init:function(){
            this._init_();
            } //-init
        } //-hooks
    // Z_DOT struct mo::__          ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.59730464008402301.2015.11.16.12.53.23.795|struct
      ,mo:{
        // Z_DOT var _name:string ::mo::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.74775464008402301.2015.11.16.12.54.17.747|+@,
        _name:'mo'
        } //-mo
    // Z_DOT struct ui::__          ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.99742564008402301.2015.11.16.12.55.24.799|struct
      ,ui:{
        // Z_DOT var _name:string      ::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.13293664008402301.2015.11.16.12.57.19.231|+@,
          _name:'ui'
        // Z_DOT struct menulink::ui::__        ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.66689664008402301.2015.11.16.12.58.18.666|struct
          ,menulink:{
            // Z_DOT var _name:string  ::menulink::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.81619764008402301.2015.11.16.12.59.51.618|+@,
              _name:'menulink'
            // Z_DOT var _ts:string    ::menulink::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.10833864008402301.2015.11.16.13.00.33.801|-,
              ,_ts:'li.navigation-link.my-account > div.podio-global-help-dropdown.hidden > div:nth-child(1)'
            // Z_DOT var _tgt:qo       ::menulink::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.85788274008402301.2015.11.16.13.08.08.758|-,
              ,_tgt:null
            // Z_DOT cmd _init_():void ::menulink::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.98794374008402301.2015.11.16.13.09.09.789|-!
              ,_init_:function(){
                var imgurl=chrome.extension.getURL('icon48.png');
                //console.log(imgurl);
                this._tgt=this._ts.q;
                var s='';
                s+='<div class="section">';
                s+='<ul>';
                s+='<li class="podio-skin image-block">';
                s+='<div class="img">';
                s+='<a  class="graphitelink" href="#">';
                s+='<img alt="Small" height="32" src="'+imgurl+'" width="32">';
                s+='</a>';
                s+='</div>';
                s+='<div class="bd"><a class="graphitelink" href="#" style="line-height:28px;">Graphite Skin for Podio</a></div>';
                s+='</li>';
                s+='</ul>';
                s+='</div>';
                this._tgt.after(s);
                '.graphitelink'.q.on('click',function(){
                  '.navigation-link.my-account.expanded'.q.removeClass('expanded');
                   __.ui.about._init();
                   //var x='about'.m;
                });
                } //-_init_
            // Z_DOT cmd _init():void  ::menulink::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.14511474008402301.2015.11.16.13.10.11.541|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-menulink
        // Z_DOT struct about::ui::__           ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.93789474008402301.2015.11.16.13.11.38.739|struct
          ,about:{
            // Z_DOT var _name:string  ::about::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.29084574008402301.2015.11.16.13.12.28.092|+@,
              _name:'about'
            // Z_DOT cmd _init_():void ::about::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.36749574008402301.2015.11.16.13.13.14.763|-!
              ,_init_:function(){
                var s='';
                var ww=650;
                var clr='color:#999;';
                var clr2='color:#D75E14';
                var imgurl=chrome.extension.getURL('icon48.png');
                s+='<div class="modal">';
                s+='  <div class="content" style="width: '+ww+'px; margin-left: -'+(ww/2)+'px; visibility: visible; top: 60px;">';
                s+='    <div class="modal-wrapper">';
                s+='      <div class="modal-header">';
                s+='        <div class="close-button" title="Close" data-modal-action="abort">';
                s+='        </div>';
                s+='        <h2>About Graphite Skin for Podio</h2>';
                s+='      </div>';
                s+='      <div class="modal-content" style="color:#888;background:rgba(30,30,30,0.9);max-height:600px;overflow-y:scroll;">';
                s+='<img src="'+imgurl+'" style="float:left;margin-right:6px;margin-bottom:6px;margin-top:-14px;" width="48">';
                s+='<h3 style="'+clr2+'">Thank you for using Graphite Skin!</h3>';
                s+='<p style="'+clr+'">This skin is an attempt at <i>perfect and <u>thorough</u></i> CSS styling of a web application &mdash; ';
                s+=' something far above what you find in normal skins, let alone in an off-the-shelf web app.';
                s+=' It is the product of intense determination to polish the CSS of an application I really admire and enjoy.</p>';
                s+='<p style="'+clr+'">';
                s+='Even though much effort has gone into this skin, there still may be flaws and sections I have missed.';
                s+=' If you discover something, drop me a note at <a href="mailto:mr@mrobbinsassoc.com">mr@mrobbinsassoc.com</a> and ';
                s+=' I will try to get it in the next update.';
                s+='</p>';
                s+='<h4 style="'+clr2+'">More than a Skin</h4>';
                s+='<p style="'+clr+'">';
                s+='Graphite Skin is more than color and styling, it <u>includes enhancements to the Podio UI</u>.';
                s+='<ul>';
                s+='<li>&bull; Many menus and items are now easier to click on</li>';
                s+='<li>&bull; Element sizes and margins have been reduced for a more compact layout</li>';
                s+='<li>&bull; Hover indicators now show when items are clickable</li>';
                s+='<li>&bull; HTML <i>Select</i> elements have been replace with an advanced search widget!</li>';
                s+='<li>&bull; Pressing <b>F7</b> will toggle the styling</li>';
                s+='<li>&bull; Pressing <b>Insert</b> will create a new Item</li>';
                s+='<li>&bull; <b>More secret keys are available if you make a donation below!</b></li>';
                s+='</ul>';
                s+='</p>';
                s+='<h4 style="'+clr2+'">The Future</h4>';
                s+='<p style="'+clr+'">';
                s+='Coming very soon will be other versions of the skin similar to Graphite.';
                s+='<ul>';
                s+='<li>&bull; <i>Sandstorm</i> &mdash; a version with a light grey palette</li>';
                s+='<li>&bull; <i>Snowblind</i> &mdash; a version with a white palette</li>';
                s+='</ul>';
                s+='</p>';
                s+='<h4 style="'+clr2+'">Graphite Supercharger</h4>';
                s+='<p style="'+clr+'">';
                s+='Also, keep an eye out for the <b>Graphite Supercharger</b> Chrome Extension for Podio, which will include:';
                s+='<ul>';
                s+='<li>&bull; A full set of Shortcut Keys to enhance your productivity with Podio</li>';
                s+='<li>&bull; Other widgets and functionality that will improve your Podio experience</li>';
                s+='</ul>';
                s+='</p>';
                s+='<h4 style="'+clr2+'">Help Me, Help You...</h4>';
                s+='<p style="'+clr+'">';
                s+='This Chrome Extension is free. If you like it, please tell other Podio users and consider making a donation of $5.00 USD';
                s+=' toward the future development of the Graphite line of extensions, including Graphite Supercharger. ';
                s+='More interest, downloads and dollars help me create the best product. Donations can be made through PayPal.';
                s+='</p>';
                s+='<h4 style="'+clr+'">Those who donate to the project will recieve a list of 10 secret shortcut keys which perform 13 actions &mdash; that will <b>work right now</b> in this skin!</h4>';
                s+='<br>';
                s+='<br>';
                if (1) {
                  s+='<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">';
                  s+='<input type="hidden" name="cmd" value="_s-xclick">';
                  s+='<input type="hidden" name="hosted_button_id" value="3THQDG5TDBTRJ">';
                  s+='<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">';
                  s+='<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">';
                  s+='</form>';
                }
                s+='<hr>';
                s+='<p style="'+clr+'">';
                s+='If you would like a custom skinning extension built for any web application, let me know at <a href="mailto:mr@mrobbinsassoc.com">mr@mrobbinsassoc.com</a>.';
                s+=' Thank you again for trying Graphite Skin for Podio.'
                s+='</p>';
                s+='<p style="'+clr+'">';
                s+='<small>Graphite Skin is an independent product and not affiliated with Podio.com</small>';
                s+='</p>';
                s+='<hr>';
                s+='<p style="'+clr2+'">';
                s+='Credits';
                s+='</p>';
                s+='<p style="'+clr+'">';
                s+='<small>Chosen Version 1.4.2<br>Select Box Enhancer for jQuery and Prototype<br>Patrick Filler for Harvest<br><a href="http://getharvest.com" target="_newtab">http://getharvest.com</a><br><a href="https://github.com/harvesthq/chosen" target="_newtab">https://github.com/harvesthq/chosen</a><br>Copyright (c) 2011-2015 Harvest <a href="http://getharvest.com" target="_newtab">http://getharvest.com</a></small>';
                s+='</p>';
                s+='<p style="'+clr+'">';
                s+='<small>TinyColor v1.1.2<br><a href="https://github.com/bgrins/TinyColor" target="_newtab">https://github.com/bgrins/TinyColor</a><br>Brian Grinstead<br>MIT License</small>';
                s+='</p>';
                s+='<p style="'+clr+'">';
                s+='<small>Underscore.js 1.8.3<br><a href="http://underscorejs.org" target="_newtab">http://underscorejs.org</a><br>(c) 2009-2015 Jeremy Ashkenas<br>DocumentCloud and Investigative Reporters & Editors</small>';
                s+='</p>';
                s+='<p style="'+clr+'">';
                s+='<small>Radio.js<br>Chainable, Dependency Free Publish/Subscribe for Javascript<br><a href="http://radio.uxder.com" target="_newtab">http://radio.uxder.com</a><br>Scott Murphy 2012 @hellocreation<br>MIT License</small>';
                s+='</p>';
                s+='      </div>';
                s+='      <div class="modal-footer">';
                s+='        <button class="button-new close-button align-right green" color="green">Close</button>';
                s+='      </div>';
                s+='    </div>';
                s+='  </div>';
                s+='</div>';
                'body'.q.append(s);
                } //-_init_
            // Z_DOT cmd _init():void  ::about::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.62827674008402301.2015.11.16.13.14.32.826|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-about
        // Z_DOT struct body::ui::__            ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.45161774008402301.2015.11.16.13.15.16.154|struct
          ,body:{
            // Z_DOT var _name:string            ::body::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.55874284008402301.2015.11.16.13.24.07.855|+@,
              _name:'body'
            // Z_DOT fn _css_container(n):string ::body::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.66090384008402301.2015.11.16.13.25.09.066|-?
              ,_css_container:function(n){
                var s='';
                s+=n+' {';
                s+='  opacity: 1;';
                s+='  transition: opacity 2500ms;';
                s+='  pointer-events: all;';
                s+='}';
                return s;
                } //-_css_container
            // Z_DOT fn _css():string            ::body::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.52947384008402301.2015.11.16.13.26.14.925|-?
              ,_css:function(){
                var s='';
                s+=this._css_container('html>body>.content-container');
                s+=this._css_container('html>body>.global-container');
                s+=this._css_container('html>body>#header-global');
                s+='html>body.presented {';
                //s+='  background-size: 6000px 6000px;';
                s+='  background-size: 0px 0px;';
                s+='  background-position: -10000px -100px;';
                s+='  transition: background-size 20000ms, background-position 5ms ease 21500ms!important;';
                //s+='  transition: background-size 5000ms!important;';
                s+='}';
                s+='html>body.presented:before {';
                s+='  -webkit-transform: scale(5, 5);';
                s+='  opacity: 0;';
                s+='  transition: opacity 2500ms, -webkit-transform 3500ms;';
                //s+='  transition: opacity 2500ms;';
                s+='}';
                return s;
                } //-_css
            // Z_DOT cmd _do_css():void          ::body::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.14372484008402301.2015.11.16.13.27.07.341|-!
              ,_do_css:function(){
                var s=this._css();
                console.log('presenting');
                __.utils.updateNamedStyle(s,'present');
                } //-_
            // Z_DOT cmd present():void          ::body::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.95376484008402301.2015.11.16.13.27.47.359|+!
              ,present:function(){
                this._do_css();
                'body'.q.addClass('presented');
                __.ui._init();
                setTimeout(function(){
                __.utils.requestTabPic();
                },3000);
                } //-present
            // Z_DOT cmd _init_():void           ::body::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.49820584008402301.2015.11.16.13.28.22.894|-!
              ,_init_:function(){
                //nada
                } //-_init_
            // Z_DOT cmd _init():void            ::body::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.39054584008402301.2015.11.16.13.29.05.093|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-body
        // Z_DOT struct content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.65549584008402301.2015.11.16.13.29.54.556|struct
          ,content_overlay:{
            // Z_DOT var _name:string ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.80826684008402301.2015.11.16.13.31.02.808|+@,
              _name:'content_overlay'
            // Z_DOT var _myid:string ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.04280784008402301.2015.11.16.13.31.48.240|-,
              ,_myid:'content-overlay:opacity'
            // Z_DOT fn _css():string ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.05425784008402301.2015.11.16.13.32.32.450|-?
              ,_css:function(v){
                var s='';
                s+='.content-overlay.background{';
                s+='background-color: rgba(0,0,0,'+v+');';
                s+='}';
                return s;
                } //-_css
            // Z_DOT cmd set(v):void ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.59081884008402301.2015.11.16.13.33.38.095|+!
              ,set:function(v){
                var that=this;
                var os={};
                var me=this._myid;
                os[me]=v;
                chrome.storage.sync.set(os,function(){
                  that.update();
                });
                } //-set
            // Z_DOT cmd adjust:void ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.40227884008402301.2015.11.16.13.34.32.204|+!
              ,adjust:function(s){
                var that=this;
                var me=this._myid;
                chrome.storage.sync.get(me, function(data){
                  var val;
                  if (Object.keys(data).length===0) {
                    var os={};
                    os[me]=0.5;
                    val=os[me];
                    chrome.storage.sync.set(os);
                  }else{
                    val=data[me];
                  }
                  val=parseFloat(val);
                  if (s==='+') {
                    val=val+0.1;
                  }else{
                    val=val-0.1;
                  }
                  val=val<0?0:val>1?1:val;
                  that.set(val);
                });
                } //-adjust
            // Z_DOT cmd update():void ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.44021984008402301.2015.11.16.13.35.12.044|+!
              ,update:function(){
                var that=this;
                var me=this._myid;
                chrome.storage.sync.get(me, function(data){
                  //console.log(data);
                  var val;
                  if (Object.keys(data).length===0) {
                    var os={};
                    os[me]=0.5;
                    val=os[me];
                    chrome.storage.sync.set(os);
                  }else{
                    val=data[me];
                  }
                  var css=that._css(val);
                  __.utils.updateNamedStyle(css,'content_overlay');
                });
                } //-update
            // Z_DOT cmd _ui():void ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.95952294008402301.2015.11.16.13.40.25.959|-!
              ,_ui:function(){
                var tgt='.content-overlay'.q;
                var has=tgt.find('#content_overlay_opacity');
                if (has.length!==0) {
                  return;
                }
                var aft=tgt.find(' .item-topbar ul.menu');
                var s='';
                s+='<div id="content_overlay_opacity">';
                s+='<a id="content_overlay_opacity_decrease" title="decrease background opacity" class="ctrl opacity dec">-</a>';
                s+='<a id="content_overlay_opacity_increase" title="increase background opacity" class="ctrl opacity inc">+</a>';
                s+='</div>';
                aft.after(s);
                } //-_ui
            // Z_DOT cmd _watch():void ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.45213694008402301.2015.11.16.13.47.11.254|-!
              ,_watch:function(){
                var that=this;
                var tgt='.content-overlay'.q;
                if (tgt.length==1) {
                  this._ui();
                }
                setTimeout(function(){
                  that._watch();
                },5000);
                } //-_watch
            // Z_DOT cmd _listen():void ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.11701794008402301.2015.11.16.13.48.30.711|-!
              ,_listen:function(){
                var that=this;
                'body'.q.on('click','.content-overlay .item-topbar .ctrl.opacity',function(evt){
                  var me=$(this);
                  if (me.hasClass('inc')) {
                    that.adjust('+');
                  }else{
                    that.adjust('-');
                  }
                });
                'body'.q.on('mouseenter',' .item-overlay-wrapper .app-item-id'
                ,function(evt){
                  console.log('enter');
                  '.item-overlay-wrapper'.q.animate({opacity:0},500,function(){});
                });
                'body'.q.on('mouseleave',' .item-overlay-wrapper .app-item-id'
                ,function(evt){
                  console.log('leave');
                  '.item-overlay-wrapper'.q.animate({opacity:1},1000,function(){});
                });
                'body'.q.on('focus',' .items-section .body .item-content .item-content-body .app-fields-list li *',function(){
                  var me=$(this);
                  me.parents('li.can-update').addClass('focus');
                  me.parents('.item-content-body').addClass('focus');
                });
                'body'.q.on('blur',' .items-section .body .item-content .item-content-body .app-fields-list li *',function(){
                  var me=$(this);
                  me.parents('li.can-update').removeClass('focus');
                  me.parents('.item-content-body').removeClass('focus');
                });
                } //-_listen
            // Z_DOT cmd _init_():void ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.70080894008402301.2015.11.16.13.50.08.007|-!
              ,_init_:function(){
                this._listen();
                this._watch();
                this.update();
                } //-_init
            // Z_DOT cmd _init():void ::content_overlay::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.29136894008402301.2015.11.16.13.51.03.192|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-content_overlay
        // Z_DOT cmd _init_():void     ::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.06130994008402301.2015.11.16.13.51.43.160|-!
          ,_init_:function(){
            this.content_overlay._init();
            //this.bye._init();
            } //-_init_
        // Z_DOT cmd _init_():void     ::ui::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.53594994008402301.2015.11.16.13.52.29.535|%!
          ,_init:function(){
            this._init_();
            } //-_init
        } //-ui
    // Z_DOT struct cmd::__         ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.72739994008402301.2015.11.16.13.53.13.727|struct
      ,cmd:{
        // Z_DOT var _name:string ::cmd::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.72613005008402301.2015.11.16.13.53.51.627|+@,
          _name:'cmd'
        } //-cmd
    // Z_DOT struct fixes::__       ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.80817005008402301.2015.11.16.13.54.31.808|struct
      ,fixes:{
        // Z_DOT var _name:string                               ::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.45123105008402301.2015.11.16.13.55.32.154|+@,
          _name:'fixes'
        // Z_DOT struct tabsets::fixes::__                                  ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.85441205008402301.2015.11.16.13.56.54.458|struct
          ,tabsets:{
            // Z_DOT var _name:string            ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.84848205008402301.2015.11.16.13.58.04.848|+@,
              _name:'tabsets'
            // Z_DOT var every:int               ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.03281305008402301.2015.11.16.13.58.38.230|+,
              ,every:3000
            // Z_DOT cmd work():void             ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.38106305008402301.2015.11.16.13.59.20.183|+!
              ,work:function(){
                var a=[
                  '#header-global'
                  ,'nav.secondary-header.subnav'
                  ,'header.app-sidebar__header'
                  ,'div.app-description'
                  ,'ul.app-views-list.standard-views'
                  ,'ul.app-views-list.public-views'
                  ,'ul.app-views-list.private-views'
                  ,'header.app-view-header'
                  ,'main.items-list'
                  ,'.items-card table.data .card-group-body td'
                ];
                var tot=0;
                a.forEach(function(v,i,a){
                  //v.q.attr('tabset',''+i);
                  v.q.each(function(){
                    var me=$(this);
                    me.attr('tabset',''+(tot));
                    tot++;
                  });
                });
                } //-work
            // Z_DOT cmd watch():void            ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.98140405008402301.2015.11.16.14.00.04.189|+!
              ,watch:function(){
                this.work();
                var that=this;
                setTimeout(function(){
                  that.watch();
                },this.every);
                } //-watch
            // Z_DOT fn find_set(q):string       ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.29523405008402301.2015.11.16.14.00.32.592|+?
              ,find_set:function(q){
                var p=q.parents();
                var ts;
                p.each(function(i,o){
                  //debugger;
                  o=$(o);
                  if (o.attr('tabset')!==undefined) {
                    ts=o.attr('tabset');
                    return false;
                  }
                });
                return ts;
                } //-find_set
            // Z_DOT fn get_nums():[int]         ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.58000505008402301.2015.11.16.14.01.40.085|+?
              ,get_nums:function(){
                var q='*[tabset]'.q;
                var a=[];
                q.each(function(){
                  var me=$(this);
                  a.push(parseInt(me.attr('tabset')));
                });
                a=a.sort(function(a,b){return a-b;});
                return a;
                } //-get_nums
            // Z_DOT fn get_next_num(ts):int     ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.20674505008402301.2015.11.16.14.02.27.602|+?
              ,get_next_num:function(ts){
                ts=parseInt(ts);
                var a=this.get_nums();
                var i=a.indexOf(ts);
                if (i===-1) {
                  if (a.length==0) {return;}
                  return a[0];
                }
                if (i===a.length-1) {
                  if (a.length==0) {return;}
                  return a[0];
                }
                return a[i+1];
                } //-get_next_num
            // Z_DOT fn get_prev_num(ts):int     ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.24316705008402301.2015.11.16.14.06.01.342|+?
              ,get_prev_num:function(ts){
                ts=parseInt(ts);
                var a=this.get_nums();
                var i=a.indexOf(ts);
                if (i===-1) {
                  if (a.length==0) {return;}
                  return a[0];
                }
                if (i===0) {
                  if (a.length==0) {return;}
                  return a[a.length-1];
                }
                return a[i-1];
                } //-get_prev_num
            // Z_DOT fn get_for(ts):qo           ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.67290805008402301.2015.11.16.14.06.49.276|+?
              ,get_for:function(ts){
                var q=('*[tabset="'+ts+'"]').q;
                var t=q.find(' *:tabbable, a:not([tabindex=-1])');
                return t;
                } //-get_for
            // Z_DOT fn nextprev_with(ts,np):int ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.38185805008402301.2015.11.16.14.07.38.183|+?
              ,nextprev_with:function(ts,np){
                var ts0=ts;
                console.log('nextprev_with',ts,np);
                np=np===undefined?'n':np;
                var nextprev=np==='n'?'next':'prev';
                var lim=0;
                while (true) {
                  lim++;
                  if(lim>20){
                    ts=ts0;
                     break;
                  }
                  ts=this['get_'+nextprev+'_num'](ts);
                  var q=this.get_for(ts);
                  if (q.length!==0) {
                    break;
                  }
                }
                return ts;
                } //-next_with
            // Z_DOT cmd next():void             ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.97829905008402301.2015.11.16.14.09.52.879|+!
              ,next:function(){
                //debugger;
                var f=':focus'.q;
                var ts;
                if (f.length!==0) {
                  ts=this.find_set(f);
                  ts=this.nextprev_with(ts,'n');
                }else{
                  ts='0';
                  var q=this.get_for(ts);
                  if (q.length===0) {
                    ts=this.nextprev_with(ts,'n');
                  }
                }
                var q=this.get_for(ts);
                if (q.length!==0) {
                  q.eq(0).focus();
                }
                } //-next
            // Z_DOT cmd prev():void             ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.16933015008402301.2015.11.16.14.10.33.961|+!
              ,prev:function(){
                var f=':focus'.q;
                var ts;
                if (f.length!==0) {
                  ts=this.find_set(f);
                  ts=this.nextprev_with(ts,'p');
                }else{
                  ts='0';
                  var q=this.get_for(ts);
                  if (q.length===0) {
                    ts=this.nextprev_with(ts,'n');
                  }
                }
                var q=this.get_for(ts);
                if (q.length!==0) {
                  q.eq(0).focus();
                }
                } //-prev
            // Z_DOT cmd _init_():void           ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.77765015008402301.2015.11.16.14.10.56.777|-!
              ,_init_:function(){
                if (__.state.isAppPage()) {
                  this.watch();
                }
                } //-_init_
            // Z_DOT cmd _init():void            ::tabsets::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.67309115008402301.2015.11.16.14.13.10.376|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-tabsets
        // Z_DOT struct task_tip::fixes::__                                 ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.03352215008402301.2015.11.16.14.13.45.330|struct
          ,task_tip:{
            // Z_DOT var _name:string ::task_tip::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.79201315008402301.2015.11.16.14.15.10.297|+@,
              _name:'task_tip'
            // Z_DOT cmd _init():void ::task_tip::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.81514315008402301.2015.11.16.14.15.41.518|-!
              ,_init_:function(){
                jQuery('.task-summary .linked-item:not([title]').each(function(){
                  var me=jQuery(this);
                  me.attr('title',me.text());
                  me.addClass('tooltip').addClass('tooltip-delayed');
                });
                } //-_init_
            // Z_DOT cmd _init():void ::task_tip::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.35387315008402301.2015.11.16.14.16.18.353|%!
              ,_init:function(){
                this._init_();
                } //-init
            } //-task_tip
        // Z_DOT struct overlap::fixes::__                                  ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.20703415008402301.2015.11.16.14.17.10.702|struct
          ,overlap:{
            // Z_DOT var _name:string  ::overlap::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.94713715008402301.2015.11.16.14.22.11.749|+@,
              _name:'overlap'
            // Z_DOT cmd _init_():void ::overlap::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.00665815008402301.2015.11.16.14.24.16.600|-!
              ,_init_:function(){
                __.data.timers.overlap=setInterval(function(){
                  jQuery('.overlap').css('width','');
                },500);
                } //-_init_
            // Z_DOT cmd _init():void  ::overlap::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.69711915008402301.2015.11.16.14.25.11.796|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-overlap
        // Z_DOT struct click_on_enter::fixes::__                           ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.72341225008402301.2015.11.16.14.30.14.327|struct
          ,click_on_enter:{
            // Z_DOT var _name:string      ::click_on_enter::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.20926225008402301.2015.11.16.14.31.02.902|+@,
              _name:'click_on_enter'
            // Z_DOT fn should():bool      ::click_on_enter::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.92321325008402301.2015.11.16.14.31.52.329|+?
              ,should:function(){
                var q=$(':focus');
                if (q.length===0) {return false;}
                var el=q[0];
                var a=__.data.click_on_enter;
                var rv=false;
                $.each(a,function(i,v){
                  var qe=v.q;
                  if (qe.length!==1) {return true;}
                  if (qe[0] === el) {rv=true;return false;}
                });
                return rv;
                } //-should
            // Z_DOT cmd register(as):void ::click_on_enter::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.20085325008402301.2015.11.16.14.32.38.002|+!
              ,register:function(as){
                if (typeof as==='string') {
                  as=[as];
                }
                var a=__.data.click_on_enter;
                $.each(as,function(i,v){
                  if (a.indexOf(v)===-1) {
                    a.push(v);
                  }
                });
                } //-register
            // Z_DOT cmd _init_():void     ::click_on_enter::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.83559325008402301.2015.11.16.14.33.15.538|-!
              ,_init_:function(){
                __.data.click_on_enter=__.data.click_on_enter?__.data.click_on_enter:[];
                } //-_init_
            // Z_DOT cmd _init():void      ::click_on_enter::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.13142425008402301.2015.11.16.14.33.44.131|%!
              ,_init:function(){
                this._init_();
                } //-_init
            }
        // Z_DOT struct app_tools_can_focus::fixes::__                      ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.74054425008402301.2015.11.16.14.34.05.047|struct
          ,app_tools_can_focus:{
            // Z_DOT var _name:string  ::app_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.84427525008402301.2015.11.16.14.36.12.448|+@,
              _name:'app_tools_can_focus'
            // Z_DOT cmd until():void  ::app_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.46071625008402301.2015.11.16.14.36.57.064|+!
              ,until:function(){
                var a=[
                '.app-view__sidebar .app-header__buttons-group .follow-action'
                ,'.app-view__sidebar .app-header__buttons-group .app-header__app-menu'
                ,'.app-view__sidebar .app-header__buttons-group .fullscreen-trigger'
                ];
                //body.space.apps.section-app.vis > div.content-container > section#wrapper.apps--new.app-view.space.apps.section-app.is-revealed > nav.app-view__sidebar > section.app-sidebar > header.app-sidebar__header > div.app-header.is-fit-to-width > div.app-header__buttons-group > div.follow-action.
                //body.space.apps.section-app.vis > div.content-container > section#wrapper.apps--new.app-view.space.apps.section-app.is-revealed > section.app-view__content > header.app-view-header.v2 > nav.app-view-header__config > div.app-header-wrapper > div.app-header > div.app-header__buttons-group > div.follow-action.
                var did=false;
                $.each(a,function(i,v){
                  if (v.q.length!==0) {
                    did=true;
                    //console.log(i,v);
                    v.q.each(function(){
                      var me=$(this);
                      //console.log(me.getDomPath());
                    });
                    v.q.attr('tabindex','0');
                  }else{
                    //console.log(i,v.q);
                  }
                  __.fixes.click_on_enter.register(v);
                });
                var that=this;
                if (!did) {
                  setTimeout( function(){that.until()},1000);
                }
                } //-until
            // Z_DOT fn should():bool  ::app_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.24786725008402301.2015.11.16.14.39.28.742|+?
              ,should:function(){
                return __.state.isAppPage();
                } //-should
            // Z_DOT cmd _init_():void ::app_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.82750335008402301.2015.11.16.14.48.25.728|-!
              ,_init_:function(){
                if (this.should()) {
                  this.until();
                }else{
                  console.error('fail should');
                }
                } //-_init_
            // Z_DOT cmd _init():void  ::app_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.80195435008402301.2015.11.16.14.50.59.108|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-app_tools_can_focus
        // Z_DOT struct view_tools_can_focus::fixes::__                     ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.16179435008402301.2015.11.16.14.51.37.161|struct
          ,view_tools_can_focus:{
            // Z_DOT var _name:string  ::view_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.70013635008402301.2015.11.16.14.53.51.007|+@,
              _name:'view_tools_can_focus'
            // Z_DOT var every:int     ::view_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.03367635008402301.2015.11.16.14.54.36.330|+,
              ,every:3000
            // Z_DOT cmd watch:void    ::view_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.68569155008402301.2015.11.16.15.19.56.586|+!
              ,watch:function(){
                var a=[
                '.app-view-header__config .app-view-tools .app-view-tools__item.layouts'
                ,'.app-view-header__config .app-view-tools .app-view-tools__item.sorting'
                ,'.app-view-header__config .app-view-tools .app-view-tools__item.options'
                ,'.app-view-header__config .app-filter-tools .app-view-tools__item.filters'
                ];
                var did=false;
                $.each(a,function(i,v){
                  if (v.q.length!==0) {
                    did=true;
                    //console.log(i,v);
                    v.q.each(function(){
                      var me=$(this);
                    });
                    v.q.attr('tabindex','0');
                  }else{
                    //console.log(i,v.q);
                  }
                  __.fixes.click_on_enter.register(v);
                });
                var that=this;
                setTimeout( function(){that.watch()},this.every);
                } //-watch
            // Z_DOT fn should():bool  ::view_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.04992255008402301.2015.11.16.15.20.29.940|+?
              ,should:function(){
                return __.state.isAppPage();
                } //-should
            // Z_DOT cmd _init_():void ::view_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.66548255008402301.2015.11.16.15.21.24.566|-!
              ,_init_:function(){
                if (this.should()) {
                  this.watch();
                }
                } //-_init_
            // Z_DOT cmd _init():void  ::view_tools_can_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.07831355008402301.2015.11.16.15.21.53.870|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-app_tools_can_focus
        // Z_DOT struct no_focus_on_hidden_app_view_composer_els::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.14119355008402301.2015.11.16.15.23.11.141|struct
          ,no_focus_on_hidden_app_view_composer_els:{
            // Z_DOT var _name:string  ::no_focus_on_hidden_app_view_composer_els::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.02843455008402301.2015.11.16.15.23.54.820|+@,
              _name:'no_focus_on_hidden_chosen'
            // Z_DOT fn should():bool  ::no_focus_on_hidden_app_view_composer_els::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.24609455008402301.2015.11.16.15.24.50.642|+?
              ,should:function(){
                return __.state.isAppPage();
                } //-should
            // Z_DOT cmd _init_():void ::no_focus_on_hidden_app_view_composer_els::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.06612555008402301.2015.11.16.15.25.21.660|-!
              ,_init_:function(){
                return;// defunct
                if (!this.should()) {return;}
                __.data.timers.no_focus_on_hidden_chosen=setInterval(function(){
                  if (__.state.isSaveViewShowing()) {
                    '.app-view-composer input, .app-view-composer a'.q.each(function(){
                      var q=$(this);
                      if (q.attr('oldtablindex')!==undefined) {
                        q.attr('tabindex',q.attr('oldtabindex'));
                      }else{
                        q.attr('tabindex','');
                      }
                    });
                  }else{
                    '.app-view-composer input,.app-view-composer a'.q.each(function(){
                      var q=$(this);
                      if (q.attr('tabindex')!==undefined) {
                        q.attr('oldtabindex',q.attr('tabindex'));
                      }else{
                      }
                      q.attr('tabindex','-1');
                    });
                  }
                },500);
                } //-_init_
            // Z_DOT cmd _init():void  ::no_focus_on_hidden_app_view_composer_els::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.55886555008402301.2015.11.16.15.26.08.855|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-no_focus_on_hidden_chosen
        // Z_DOT struct handle_sidebar_hidden_focus::fixes::__              ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.63919555008402301.2015.11.16.15.26.31.936|struct
          ,handle_sidebar_hidden_focus:{
            // Z_DOT var _name:string  ::handle_sidebar_hidden_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.83574655008402301.2015.11.16.15.27.27.538|+@,
              _name:'handle_sidebar_hidden_focus'
            // Z_DOT var every:int     ::handle_sidebar_hidden_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.53349655008402301.2015.11.16.15.28.14.335|+,
              ,every:5000
            // Z_DOT should():bool     ::handle_sidebar_hidden_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.32721755008402301.2015.11.16.15.28.32.723|+?
              ,should:function(){
                return __.state.isAppPage();
                } //-should
            // Z_DOT cmd work():void   ::handle_sidebar_hidden_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.60453755008402301.2015.11.16.15.28.55.406|+!
              ,work:function(){
                var a=[
                   {a:['.app-view__sidebar a.appname','.app-view__content a.appname']}
                  ,{a:['.app-view__sidebar .follow-action','.app-view__content .follow-action']}
                  ,{a:['.app-view__sidebar .app-header__app-menu','.app-view__content .app-header__app-menu']}
                  ,{a:['.app-view__sidebar .fullscreen-trigger','.app-view__content .fullscreen-trigger']}
                ];
                var open=__.state.isFilterSidebarOpen();
                $.each(a,function(i,v){
                  var a=v.a;
                  var b={
                    'true':['0','-1']
                    ,'false':['-1','0']
                  }
                  a[0].q.attr('tabindex',b[''+open][0]);
                  a[1].q.attr('tabindex',b[''+open][1]);
                });
                var vv=open?'0':'-1';
                '.app-view__sidebar a'.q.attr('tabindex',vv);
                '.app-view__sidebar input'.q.attr('tabindex',vv);
                } //-work
            // Z_DOT cmd repeat():void ::handle_sidebar_hidden_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.07937755008402301.2015.11.16.15.29.33.970|+!
              ,repeat:function(){
                //console.log(arguments.callee);
                var that=this;
                that.work();
                setTimeout(function(){
                  that.repeat();
                },that.every);
                } //-repeat
            // Z_DOT cmd _init_():void ::handle_sidebar_hidden_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.72390855008402301.2015.11.16.15.30.09.327|-!
              ,_init_:function(){
                var that=this;
                setTimeout(function(){
                  if (0) {
                    $(document).on('focus','*',0,function(){
                      var me=$(this);
                      console.info(me.getDomPath());
                    });
                  }
                  if (that.should()) {
                    that.repeat();
                  }
                },3000);
                } //-_init_
            // Z_DOT cmd _init():void  ::handle_sidebar_hidden_focus::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.05703855008402301.2015.11.16.15.30.30.750|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-handle_sidebar_hidden_focus
        // Z_DOT struct footer_show_more_items_no_tabstop::fixes::__        ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.63477855008402301.2015.11.16.15.31.17.436|struct
          ,footer_show_more_items_no_tabstop:{
            // Z_DOT var _name:string  ::footer_show_more_items_no_tabstop::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.28481955008402301.2015.11.16.15.31.58.482|+@,
              _name:'footer_show_more_items_no_tabstop'
            // Z_DOT var every:int     ::footer_show_more_items_no_tabstop::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.09494955008402301.2015.11.16.15.32.29.490|+,
              ,every:5000
            // Z_DOT fn should():bool  ::footer_show_more_items_no_tabstop::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.17161065008402301.2015.11.16.15.33.36.171|+?
              ,should:function(){
                return 'body.space.apps.section-app'.exists;
                } //-should
            // Z_DOT cmd work():void   ::footer_show_more_items_no_tabstop::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.45804065008402301.2015.11.16.15.34.00.854|+!
              ,work:function(){
                'section.app-view__content > section.app-wrapper__content > footer.show-more-items > a:not[tabindex]'.q.attr('tabindex','-1');
                } //-work
            // Z_DOT cmd repeat():void ::footer_show_more_items_no_tabstop::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.20626065008402301.2015.11.16.15.34.22.602|+!
              ,repeat:function(){
                //console.log(arguments.callee);
                var that=this;
                that.work();
                setTimeout(function(){
                  that.repeat();
                },that.every);
                } //-repeat
            // Z_DOT cmd _init_():void ::footer_show_more_items_no_tabstop::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.86028065008402301.2015.11.16.15.34.42.068|-!
              ,_init_:function(){
                var that=this;
                setTimeout(function(){
                  if (that.should()) {
                    that.repeat();
                  }
                },3000);
                } //-_init_
            // Z_DOT cmd _init():void  ::footer_show_more_items_no_tabstop::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.07881165008402301.2015.11.16.15.35.18.870|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-footer_show_more_items_no_tabstop
        // Z_DOT struct addScrollClasses::fixes::__                         ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.68804165008402301.2015.11.16.15.35.40.886|struct
          ,addScrollClasses:{
            // Z_DOT var _name:string  ::addScrollClasses::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.99453265008402301.2015.11.16.15.37.15.499|+@,
              _name:'addScrollClasses'
            // Z_DOT cmd _init_():void ::addScrollClasses::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.19626265008402301.2015.11.16.15.37.42.691|-!
              ,_init_:function(){
                $(window).scroll(function() {
                  'windowScroll'._bc();
                  var scroll = $(window).scrollTop();
                  if (scroll >= 1) {
                    if ('body.scrolled'.q.length==0) {
                      'body'.q.addClass("scrolled");
                    }
                  } else {
                    'body'.q.removeClass("scrolled");
                  }
                });
                } //-_init_
            // Z_DOT cmd _init():void  ::addScrollClasses::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.13888265008402301.2015.11.16.15.38.08.831|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-addScrollClasses
        // Z_DOT struct stream_composer_css::fixes::__                      ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.18382365008402301.2015.11.16.15.38.48.381|struct
          ,stream_composer_css:{
            // Z_DOT var _name:string     ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.26994565008402301.2015.11.16.15.42.29.962|+@,
              _name:'stream_composer_css'
            // Z_DOT var _try:int         ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.58058565008402301.2015.11.16.15.43.05.085|-,
              ,_try:0
            // Z_DOT var _max_tries:int   ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.40250665008402301.2015.11.16.15.43.25.204|-,
              ,_max_tries:20
            // Z_DOT var _every:int       ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.09962665008402301.2015.11.16.15.43.46.990|-,
              ,_every:10*1000
            // Z_DOT fn _amt:int          ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.33544665008402301.2015.11.16.15.44.04.533|-?
              ,_amt:function(h){
                var base=306-128;
                return (base+h)*-1;
                } //-_amt
            // Z_DOT fn _get_css():string ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.87659665008402301.2015.11.16.15.44.55.678|-?
              ,_get_css:function(){
                var it='.identity-tile'.q;
                if (it.length==0) {return '';}
                var h=it.height();
                var s='';
                var amt=this._amt(h);
                s+='html.space.spaces.dashboard body.scrolled #stream_composer{';
                s+="\n";
                s+='  margin-top:'+amt+'px;';
                s+="\n";
                s+='  transition: margin-top 2s;';
                s+="\n";
                s+='}';
                s+="\n";
                return s;
                } //-_get_css
            // Z_DOT cmd _do_css():void   ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.96834765008402301.2015.11.16.15.45.43.869|-!
              ,_do_css:function(){
                var that=this;
                var s=this._get_css();
                if (s!=='') {
                  __.utils.updateNamedStyle(s,'stream_composer_css');
                }
                setTimeout(function(){that._do_css()},this._every);
                } //-_do_css
            // Z_DOT cmd _wait():void     ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.86428765008402301.2015.11.16.15.46.22.468|-!
              ,_wait:function(){
                var that=this;
                var wait=false;
                function do_wait(){
                  that._try++;
                  if (that._try>=that._max_tries) {
                    return;
                  }
                  setTimeout(function(){
                    that._wait();
                  },1000);
                }
                var bod='body'.q;
                if(bod.length==0){do_wait();return;}
                if (!bod.hasClass('dashboard')) {do_wait();return;}
                if (!'.identity-tile'.q.length==1) {do_wait();return;}
                this._do_css();
                } //-_wait
            // Z_DOT cmd _init_():void    ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.45114865008402301.2015.11.16.15.47.21.154|-!
              ,_init_:function(){
                this._wait();
                } //-_init_
            // Z_DOT cmd _init():void     ::stream_composer_css::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.35996865008402301.2015.11.16.15.47.49.953|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-stream_composer_css
        // Z_DOT struct inner_height::fixes::__                             ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.44815965008402301.2015.11.16.15.49.11.844|struct
          ,inner_height:{
            // Z_DOT var _name:string     ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.27203075008402301.2015.11.16.15.50.30.272|+@,
              _name:'inner_height'
            // Z_DOT var _try:int         ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.13556075008402301.2015.11.16.15.51.05.531|-!
              ,_try:0
            // Z_DOT var _tgt:qs          ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.87279075008402301.2015.11.16.15.51.37.278|-,
              ,_tgt:'html #wrapper .main-content>.inner'
            // Z_DOT var _max_tries:int   ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.59009518008402301.2015.11.16.22.39.50.095|-,
              ,_max_tries:20
            // Z_DOT fn _get_css():string ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.66821618008402301.2015.11.16.22.40.12.866|-?
              ,_get_css:function(){
                var it=$(window);
                if (it.length==0) {return '';}
                var off=0;
                if ('.content-container'.q.length===1) {
                  off='.content-container'.q.offset().top;
                }
                var h=it.height();
                var s='';
                var amt=h-off;
                s+=this._tgt+'{';
                s+="\n";
                s+='  min-height:'+amt+'px;';
                s+="\n";
                s+='}';
                s+="\n";
                return s;
                } //-_get_css
            // Z_DOT cmd _do_css():void   ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.82264618008402301.2015.11.16.22.40.46.228|-!
              ,_do_css:function(){
                var that=this;
                var s=this._get_css();
                if (s!=='') {
                  __.utils.updateNamedStyle(s,'inner_height_css');
                }
                } //-_do_css
            // Z_DOT cmd _start():void    ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.06239618008402301.2015.11.16.22.41.33.260|-!
              ,_start:function(){
                var that=this;
                var w=$(window);
                w.on('resize',U.debounce(function(){
                  that._do_css();
                },1000));
                this._do_css();
                } //-_start
            // Z_DOT cmd _wait():void     ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.20132718008402301.2015.11.16.22.42.03.102|-!
              ,_wait:function(){
                var that=this;
                var wait=false;
                function do_wait(){
                  that._try++;
                  if (that._try>=that._max_tries) {
                    return;
                  }
                  setTimeout(function(){
                    that._wait();
                  },1000);
                }
                var tgt=this._tgt.q;
                if(tgt.length==0){do_wait();return;}
                setTimeout(function(){
                  that._start();
                },1000);
                } //-_wait
            // Z_DOT cmd _init_():void    ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.92235718008402301.2015.11.16.22.42.33.229|-!
              ,_init_:function(){
                this._wait();
                } //-_init_
            // Z_DOT cmd _init():void     ::inner_height::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.43147718008402301.2015.11.16.22.42.54.134|%!
              ,_init:function(){
                this._init_();
                } //-_init
            } //-inner_height
        // Z_DOT cmd _init_():void                              ::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.58900818008402301.2015.11.16.22.43.20.985|-!
          ,_init_:function(){
            this.tabsets._init();
            this.task_tip._init();
            this.overlap._init();
            this.no_focus_on_hidden_app_view_composer_els._init();
            this.click_on_enter._init();
            this.app_tools_can_focus._init();
            this.view_tools_can_focus._init();
            this.handle_sidebar_hidden_focus._init();
            this.addScrollClasses._init();
            this.stream_composer_css._init();
            this.inner_height._init();
            } //-_init_
        // Z_DOT cmd _init():void                               ::fixes::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.62574818008402301.2015.11.16.22.44.07.526|%!
          ,_init:function(){
            this._init_();
            } //-_init
        } //-fixes
    // Z_DOT struct chosen::__      ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.27168818008402301.2015.11.16.22.44.46.172|struct
      ,chosen:{
        // Z_DOT var _name:string  ::chosen::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.14612918008402301.2015.11.16.22.45.21.641|+@,
          _name:'chosen'
        // Z_DOT cmd _init_():void ::chosen::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.24325918008402301.2015.11.16.22.45.52.342|-!
          ,_init_:function(){
            __.data.timers.chosen=setInterval(function(){
              //jQuery('select:not(.is-handled)').chosen().addClass('is-handled');
              jQuery('select:not(.hidden):not(.is-handled)').each(function(o){
                var me=jQuery(this);
                var opts={
                  search_contains:true
                  ,inherit_select_classes:true
                }
                var id=me.attr('id');
                if (id!==undefined) {
                  if (id.indexOf('_recurrence_')!=-1) {return;}
                }
                if (!me.is(':visible')) {return;}
                //console.log('chosen',me[0]);
                me.chosen(opts).addClass('is-handled');
                setTimeout(function(){
                  var q=jQuery('#new_app_store_share .podio-dropdown.podio-dropdown-common-wrapper');
                  //console.log('chosen hide #new_app_store_share .podio-dropdown.podio-dropdown-common-wrapper',q.length);
                  q.css('display','none');
                  jQuery('#app_store_share_functional_category_id_chosen, #app_store_share_vertical_category_id_chosen').css('width','215px');
                },2000);
                if (1) {
                  me.change(function(){
                    var me=jQuery(this);
                    if (me.hasClass('temp')) {
                      //console.log('me have temp');
                      return;
                    }
                    me.addClass('temp');
                    var evt = document.createEvent("HTMLEvents");
                    evt.initEvent("change", true, true);
                    this.dispatchEvent(evt);
                    setTimeout(function(){
                      //console.log('me del temp');
                      me.removeClass('temp');
                    },50);
                    //for (var i in React.addons) {
                    //  console.log(i);
                    //}
                    //console.info(this.value);
                    //this.value='Category';
                    //React.addons.TestUtils.Simulate.change(this,{target:{value:this.value}});
                  });
                }
              });
              //jQuery('.report-composer-component__config-section').keyup()
            },100);
            } //-_init_
        // Z_DOT cmd _init():void  ::chosen::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.92097918008402301.2015.11.16.22.46.19.029|%!
          ,_init:function(){
            this._init_();
            } //-init
        } //-chosen
    // Z_DOT struct state::__       ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.06431028008402301.2015.11.16.22.46.53.460|struct
      ,state:{
        // Z_DOT var _name:string ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.49115028008402301.2015.11.16.22.47.31.194|+@,
          _name:'state'
        // Z_DOT fn isViewLoading():bool ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.41159028008402301.2015.11.16.22.48.15.114|+?
        ,isViewLoading:function(){
          return 'main.items-list.overlay-loading-big-top'._one;
          } //-isViewLoading
        // Z_DOT fn isBadgeViewThere():bool       ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.07306128008402301.2015.11.16.22.49.20.370|+?
          ,isBadgeViewThere:function(){
            return 'section.app-badge-layout-component'._one;
            } //-isBadgeViewThere
        // Z_DOT fn isTableViewThere():bool       ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.41201228008402301.2015.11.16.22.50.10.214|+?
          ,isTableViewThere:function(){
            return 'div.items-table.items-container'._one;
            } //-isTableViewThere
        // Z_DOT fn isCardViewThere():bool        ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.38647228008402301.2015.11.16.22.51.14.683|+?
          ,isCardViewThere:function(){
            return 'div.items-card.items-container'._one;
            } //-isCardViewThere
        // Z_DOT fn isActivityViewThere():void    ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.26060328008402301.2015.11.16.22.51.46.062|+?
          ,isActivityViewThere:function(){
            return 'ul.content.stream-view'._one;
            } //-isActivityViewThere
        // Z_DOT fn isCalendarViewThere():bool    ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.61593328008402301.2015.11.16.22.52.19.516|+?
          ,isCalendarViewThere:function(){
            return 'div.items-fullcalendar'._one;
            } //-isCalendarViewThere
        // Z_DOT fn isAnyViewThere():bool         ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.44690428008402301.2015.11.16.22.53.29.644|+?
          ,isAnyViewThere:function(){
            return this.isBadgeViewThere()
                 ||this.isTableViewThere()
                 ||this.isCardViewThere()
                 ||this.isActivityViewThere()
                 ||this.isCalendarViewThere();
            } //-isAnyViewThere
        // Z_DOT fn isNoItems():bool              ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.15745428008402301.2015.11.16.22.54.14.751|+?
          ,isNoItems:function(){
            return '.app-view__content .items-list .warning'.q.length===1;
            } //-isNoItems
        // Z_DOT fn isAppViewLoaded():bool        ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.11288428008402301.2015.11.16.22.54.48.211|+?
          ,isAppViewLoaded:function(){
            if (this.isViewLoading()) {
              return false;
            }
            return this.isAnyViewThere();
            } //-isAppViewLoaded
        // Z_DOT fn isBadgeViewSelected():bool    ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.34610528008402301.2015.11.16.22.55.01.643|+?
          ,isBadgeViewSelected:function(){
            return '.app-settings-dropdown-list li[data-id="badge"].current'._one;
            } //-isBadgeViewSelected
        // Z_DOT fn isTableViewSelected():bool    ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.76311528008402301.2015.11.16.22.55.11.367|+?
          ,isTableViewSelected:function(){
            return '.app-settings-dropdown-list li[data-id="table"].current'._one;
            } //-isTableViewSelected
        // Z_DOT fn isCardViewSelected():bool     ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.17302528008402301.2015.11.16.22.55.20.371|+?
          ,isCardViewSelected:function(){
            return '.app-settings-dropdown-list li[data-id="card"].current'._one;
            } //-isCardViewSelected
        // Z_DOT fn isActivityViewSelected():bool ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.00143528008402301.2015.11.16.22.55.34.100|+?
          ,isActivityViewSelected:function(){
            return '.app-settings-dropdown-list li[data-id="stream"].current'._one;
            } //-isActivityViewSelected
        // Z_DOT fn isCalendarViewSelected():bool ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.25334528008402301.2015.11.16.22.55.43.352|+?
          ,isCalendarViewSelected:function(){
            return '.app-settings-dropdown-list li[data-id="calendar"].current'._one;
            } //-isCalendarViewSelected
        // Z_DOT fn isAppPage():bool              ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.15815528008402301.2015.11.16.22.55.51.851|+?
          ,isAppPage:function(){
            return 'body.space.apps.section-app'.exists;
            } //-isAppPage
        // Z_DOT fn isInEditor():bool             ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.56785528008402301.2015.11.16.22.55.58.765|+?
          ,isInEditor:function(){
            var q=':focus'.q;
            var tn=q.prop('tagName');
            if (['INPUT','TEXTAREA'].indexOf(tn)!==-1) {
              return true;
            }
            q='div.mce-tinymce'.q;
            if(q.length>0 && q.is(':visible')){
              return true;
            }
            return false;
            } //-inEditor
        // Z_DOT fn isSaveViewShowing():bool      ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.08767528008402301.2015.11.16.22.56.16.780|+?
          ,isSaveViewShowing:function(){
            if ('.app-view-composer'.qo().hasClass('is-not-visible')) {
              return false;
            }
            return true;
            } //-isSaveViewShowing
        // Z_DOT fn isFilterSidebarOpen():bool    ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.99278528008402301.2015.11.16.22.56.27.299|+?
          ,isFilterSidebarOpen:function(){
            return '.app-view__sidebar:not(.is-collapsed)'.q.length!==0;
            } //-isFilterSidebarOpen
        // Z_DOT fn isPickerFocused():bool        ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.10349528008402301.2015.11.16.22.56.34.301|+?
          ,isPickerFocused:function(){
            var q=':focus'.q;
            var tn=q.prop('tagName');
            if (tn!=='INPUT') {
              return false;
            }
            if (q.getDomPath().indexOf('#picker')===-1) {
              return false;
            }
            return true;
            } //-isPickerFocused
        // Z_DOT fn isSidebarCollapsed():bool     ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.83831628008402301.2015.11.16.22.56.53.838|+?
          ,isSidebarCollapsed:function(){
            return '.is-sidebar-collapsed'._one;
            } //-isSidebarCollapsed
        // Z_DOT fn isFullscreen():bool           ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.42212628008402301.2015.11.16.22.57.01.224|+?
          ,isFullscreen:function(){
            return 'body.section-app.fullscreen'._one;
            } //-isFullscreen
        // Z_DOT fn isAppMenuShowing():bool       ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.10372628008402301.2015.11.16.22.57.07.301|+?
          ,isAppMenuShowing:function(){
            return '.simple-balloon.app-box-supermenu'.q.is(':visible');
            } //-isAppMenuShowing
        // Z_DOT fn isFiltersDialogVisible():bool ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.50933628008402301.2015.11.16.22.57.13.905|+?
          ,isFiltersDialogVisible:function(){
            return '.simple-balloon.app-filters-popover'.q.is(':visible');
            } //-isFiltersDialogVisible
        // Z_DOT fn isViewComposerVisible():bool  ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.60915628008402301.2015.11.16.22.57.31.906|+?
          ,isViewComposerVisible:function(){
            return '.app-view-composer.is-not-visible'.q.length===0&&'.app-view-composer'.q.length===1;
            } //-isViewComposerVisible
        // Z_DOT cmd _init_():void                ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.81746628008402301.2015.11.16.22.57.44.718|-!
          ,_init_:function(){
            // nada
            } //-_init_
        // Z_DOT cmd _init():void                 ::state::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.05549628008402301.2015.11.16.22.58.14.550|%!
          ,_init:function(){
            this._init_();
            } //-_init
        } //-state
    // Z_DOT struct event::__       ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.92292728008402301.2015.11.16.22.58.49.229|struct
      ,event:{
        // Z_DOT var _name:string ::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.42826728008402301.2015.11.16.22.59.22.824|+@,
          _name:'event'
        // Z_DOT struct dom::event::__        ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.05679728008402301.2015.11.16.22.59.57.650|struct
          ,dom:{
            // Z_DOT var _name:string              ::dom::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.25604828008402301.2015.11.16.23.00.40.652|+@,
              _name:'dom'
            // Z_DOT evt dom_removed(event):void   ::dom::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.19466828008402301.2015.11.16.23.01.06.491|+\
              ,node_removed:function(event){
                } //-node_removed
            // Z_DOT evt node_inserted(event):void ::dom::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.08650928008402301.2015.11.16.23.01.45.680|+\
              ,node_inserted:function(event){
                } //-node_inserted
            } //-dom
        // Z_DOT struct kbd::event::__        ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.68025928008402301.2015.11.16.23.02.32.086|struct
          ,kbd:{
            // Z_DOT var _name:string      ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.26732038008402301.2015.11.16.23.03.43.762|+@,
              _name:'kbd'
            // Z_DOT cmd tryClick(q):void  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.25695038008402301.2015.11.16.23.04.19.652|+!
              ,tryClick:function(q){
                return __.utils.tryClick(q);
                } //-tryClick
            // Z_DOT evt k_cau9(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.89442138008402301.2015.11.16.23.05.24.498|+\
              ,k_cau9:function(evt){
                __.fixes.tabsets.next();
                return __.utils.cancel(evt);
                } //-k_ca9
            // Z_DOT evt k_casu9(evt):bool ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.79896138008402301.2015.11.16.23.06.09.897|+\
              ,k_casu9:function(evt){
                __.fixes.tabsets.prev();
                return __.utils.cancel(evt);
                } //-k_cas9
            // Z_DOT evt k_u13(evt):bool   ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.13851238008402301.2015.11.16.23.06.55.831|+\
              ,k_u13:function(evt){// KEY enter end edit
                if (__.fixes.click_on_enter.should()) {
                  var q=':focus'.q;
                  this.tryClick(q);
                  return __.utils.cancel(evt);
                }
                } //-k_u13
            // Z_DOT evt k_cu13(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.00628238008402301.2015.11.16.23.08.02.600|+\
              ,xk_cu13:function(evt){// KEY enter.c
                if (__.state.isPickerFocused()) {
                  //console.log('set picker');
                  //alert('sp');
                  __.data.picker_ctrl_enter=true;
                  setTimeout(function(){
                    __.data.picker_ctrl_enter=false;
                  },500);
                }
                } //-k_cu13
            // Z_DOT evt k_u118(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.90289238008402301.2015.11.16.23.08.18.209|+\
              ,k_u118:function(e){// KEY F7
                __.style.toggleMain();
                return __.utils.cancel(e);
                } //-k_u118
            // Z_DOT evt k_su118(evt):bool ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.04350338008402301.2015.11.16.23.08.25.340|+\
              ,k_su118:function(e){// KEY F7.s
                var q=':focus'.q;
                if (q.length!==0) {
                  console.log(q[0],q.getDomPath());
                }else{
                  console.log('no focus elem');
                }
                return __.utils.cancel(e);
                } //-k_u118
            // Z_DOT evt k_u45(evt):bool   ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.50341338008402301.2015.11.16.23.08.34.305|+\
              ,k_u45:function(e){// KEY insert new item
                var q="nav.app-view-header__config > a[href$='/new']".q;
                if (q.length===1) {
                  q.focus();
                  setTimeout(function(){
                    q.simulate('click');//q.click();
                  },1000);
                  return __.utils.cancel(e);
                }
                return;
                } //-k_u45
            // Z_DOT evt k_cd83(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.83332338008402301.2015.11.16.23.08.43.338|+\
              ,k_cd83:function(evt){
                var btn='button.save-item-button';
                if (btn.q.is(':visible')) {
                  btn.q.simulate('click');
                  return __.utils.cancel(evt);
                  //console.log(btn,'visible');
                }
                btn='.save-settings-button';
                if (btn.q.is(':visible')) {
                  btn.q.simulate('click');
                  return __.utils.cancel(evt);
                }
                btn='.items-section .field-picker .field-picker-footer .finish-template-edit-button';
                if (btn.q.is(':visible')) {
                  btn.q.simulate('click');
                  return __.utils.cancel(evt);
                }
                var view_composer_visible=__.state.isViewComposerVisible();
                if (view_composer_visible) {
                  btn='.app-view-composer__footer.l-footer > a.app-view-composer__done-button.green';
                  btn.q.simulate('click');
                  return __.utils.cancel(evt);
                }else{
                  btn='.unsaved-app-view-bar-component > div > div.img-ext > div > div.bd > a.green';
                  if (btn.q.length===1) {
                    btn.q.simulate('click');
                    return __.utils.cancel(evt);
                  }
                }
                //return __.utils.cancel(evt);
                } //-k_cd83
            // Z_DOT evt k_ad13(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.72743338008402301.2015.11.16.23.08.54.727|+\
              ,k_ad13:function(evt){
                //'a.app-settings'
                var sm='.app-box-supermenu'.q;
                if (!sm.is(':visible')) {
                  '.util-button-new.app-box-supermenu-toggle'.q.simulate('click');
                  setTimeout(function(){
                    '.app-settings'.q.simulate('click');
                  },500);
                }else{
                  '.app-settings'.q.simulate('click');
                }
                (function(){
                  tryct=0;
                  function dofoc(){
                    tryct++;
                    if (tryct>20) {
                      return;
                    }
                    if ('input[name="config.name"]'.q.length===0) {
                      setTimeout(dofoc,100);
                      return;
                    }
                    'input[name="config.name"]'.q.focus();
                  }
                  dofoc();
                })();
                return __.utils.cancel(evt);
                } //-k_ad13
            // Z_DOT evt k_ad113(evt):bool ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.70135338008402301.2015.11.16.23.09.13.107|+\
              ,k_ad113:function(evt){
                var overlay='.item-overlay'.q;
                if (overlay.length===1) {
                  'edit-template'.q.simulate('click');
                  return __.utils.cancel(evt);
                }
                var sm='.app-box-supermenu'.q;
                if (!sm.is(':visible')) {
                  '.util-button-new.app-box-supermenu-toggle'.q.simulate('click');
                  setTimeout(function(){
                    '.modify-template-button'.q.simulate('click');
                  },500);
                }else{
                  '.modify-template-button'.q.simulate('click');
                }
                } //-k_ad113
            // Z_DOT evt k_d187(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.52086338008402301.2015.11.16.23.09.28.025|+\
              ,k_d187:function(evt){
                if (!this._pfx_220) {
                  return;
                }
                delete this._pfx_220;
                var foc='.app-stream a:eq(0)'.q;
                foc.focus();
                return __.utils.cancel(evt);
                } //-k_d187
            // Z_DOT evt k_d65(evt):bool   ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.89838338008402301.2015.11.16.23.09.43.898|+\
              ,k_d65:function(evt){
                if (!this._pfx_220) {
                  return;
                }
                delete this._pfx_220;
                var foc='.app-stream a:eq(0)'.q;
                foc.focus();
                return __.utils.cancel(evt);
                } //-k_d65
            // Z_DOT evt k_d82(evt):bool   ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.92099338008402301.2015.11.16.23.09.59.029|+\
              ,k_d82:function(evt){
                if (!this._pfx_220) {
                  return;
                }
                delete this._pfx_220;
                var foc='.show-reports-trigger:eq(0)'.q;
                foc.simulate('click');
                return __.utils.cancel(evt);
                } //-k_d82
            // Z_DOT evt k_d219(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.89841438008402301.2015.11.16.23.10.14.898|+\
              ,k_d219:function(evt){
                if (!this._pfx_220) {
                  return;
                }
                delete this._pfx_220;
                var foc='.app-views-list:eq(0)'.q;
                foc.find(' a:eq(0)').focus();
                return __.utils.cancel(evt);
                } //-k_d219
            // Z_DOT evt k_d221(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.40492438008402301.2015.11.16.23.10.29.404|+\
              ,k_d221:function(evt){
                if (!this._pfx_220) {
                  return;
                }
                delete this._pfx_220;
                var foc='table.data td[tabset] a:eq(0), a.badge-component:eq(0), .stream-view a:eq(0)'.q;
                foc.focus();
                return __.utils.cancel(evt);
                } //-k_d221
            // Z_DOT evt k_d220(evt):bool  ::kbd::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.70534438008402301.2015.11.16.23.10.43.507|+\
              ,k_d220:function(evt){
                var that=this;
                var foc=':focus'.q;
                if (foc.length===1) {
                  var tn=foc.prop('tagName').toLowerCase();
                  if (tn=='input') {
                    if (foc.attr('type')==='text'||foc.attr('type')==='password') {
                      return;
                    }
                  }
                  if (tn==='textarea') {
                    return;
                  }
                }
                if (this._pfx_220) {
                  delete this._pfx_220;
                  __.fixes.tabsets.next();
                }else{
                  this._pfx_220=true;
                  setTimeout(function(){
                    delete that._pfx_220;
                  },2000);
                }
                return __.utils.cancel(evt);
                //console.log('k_d220');
                } //-k_d220
            } //-kbd
        // Z_DOT struct doc::event::__        ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.97337438008402301.2015.11.16.23.11.13.379|struct
          ,doc:{
            // Z_DOT var _name:string            ::doc::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.76311538008402301.2015.11.16.23.11.51.367|+@,
              _name:'doc'
            // Z_DOT struct _keysdown::doc::event::__             ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.56345538008402301.2015.11.16.23.12.34.365|struct
              ,_keysdown:{
                } //-_keysdown
            // Z_DOT fn cas():string             ::doc::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.93078538008402301.2015.11.16.23.13.07.039|+?
              ,cas:function(){
                var cas='';
                if (__.event.doc._keysdown._17!==undefined) {cas+='c';}
                if (__.event.doc._keysdown._18!==undefined) {cas+='a';}
                if (__.event.doc._keysdown._16!==undefined) {cas+='s';}
                return cas;
                } //-cas
            // Z_DOT cmd _do_call(code,evt):void ::doc::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.25352638008402301.2015.11.16.23.13.45.352|-!
              ,_do_call:function(code,evt){
                if (__.data.showkeys) {console.log('code:'+code);}
                //console.log('code:'+code);
                code._bc({evt:evt});
                if (typeof __.event.kbd[code]==='function') {
                  //console.log('press:'+code);
                  if (this._check_call(__.event.kbd[code])) {
                    return __.event.kbd[code](evt);
                  }
                  return;
                }else if (jQuery.isArray(__.event.kbd[code])) {
                  var a=__.event.kbd[code];
                  var x;
                  for (x=0;x<a.length;x++) {
                    if (typeof a[x]==='function') {
                      console.log('press:'+x+' '+code);
                      if (!this._check_call(a[x])) {
                        console.log('item failed _check_call :'+x+' '+code);
                        continue;
                      }
                      var rv=a[x](evt);
                      if (rv!==undefined) {
                        if (rv===false) {
                          // return 'continue'
                          return rv;
                        }
                      }
                    }
                  }
                }
                } //-_do_call
            // Z_DOT fn _check_call(fo):bool     ::doc::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.24897638008402301.2015.11.16.23.14.39.842|-?
              ,_check_call:function(fo){
                if (fo.me===undefined) {
                  return true;
                }
                console.log('_check_call:j:'+fo.me.k.j);
                if (fo.me.k.j.indexOf('c')===-1&&fo.me.k.j.indexOf('a')===-1&&fo.me.k.j.indexOf('s')===-1) {
                  console.log('_check_call:plainkey?:'+fo.me.k.j);
                  // plain key
                  var kc=fo.me.k.j.substr(1);
                  kc=parseInt(kc);
                  console.log('_check_call:plainkey?:kc:'+kc);
                  // enter esc space 0-9
                  var aa=[13,27,32,48,49,50,51,52,53,54,55,56
                   // - = bs
                   ,189,187,8
                   // a-z
                   ,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90
                   // [ ] \
                   ,219,221,220
                   // ; '
                   ,186,222
                   // ,./
                   ,188,190,191
                   // ins hom pu del end pd
                   ,45,36,33,46,35,34
                   // left right up down
                   ,37,39,38,40
                   // f1-f12  3  4  5    6   7   8   9  10   11  12
                   //,112,113,114,115,116,117,118,119,120,121,122,123
                   // num / * - + .
                   ,111,106,109,107,110
                   // num 0-9    5   6   7   8   9
                   ,96,97,98,99,100,101,102,103,104
                   ];
                  console.log('_check_call:aa.indexOf('+kc+'):'+aa.indexOf(kc));
                  if (aa.indexOf(kc)!=-1) {
                    if (__.state.inEditor()) {
                      console.log('exit inEditor:'+kc+' '+fo.me.n);
                      return false;
                    }
                  }
                }
                // return true if should call
                var w=fo.me.W;
                //console.log('fo,fo.me:',fo,fo.me);
                if (w===undefined) {
                  return true;
                }
                //console.log('w',w);
                w=w.split(',');
                //console.log('w',w);
                //w=__.utils.cleanFor(w,'string');
                //console.log('w',w);
                //console.log('w.length',w.length);
                var ay=[];
                var an=[];
                var x;
                for (x=0;x<w.length;x++) {
                  var i=w[x];
                  if (typeof i!=='string') {
                    continue;
                  }
                  if (i.substr(0,1)=='!') {
                    an.push(i.substr(1));
                  }else{
                    ay.push(i);
                  }
                }
                //console.log('ay',ay,'an',an);
                //ay=__.utils.cleanFor(ay,'string');
                //an=__.utils.cleanFor(an,'string');
                //console.log('ay',ay,'an',an);
                function tryit(s){
                  var f=__.state['is'+s];
                  if (typeof f!=='function') {
                    console.error('state Fn not found:is'+s+'()');
                    return false;
                  }
                  var rv=__.state['is'+s]();
                  return rv;
                }
                var yy=false;
                for (x=0;x<ay.length;x++) {
                  if (tryit(ay[x])) {
                    yy=true;
                  }
                }
                if (ay.length===0) {
                  yy=true;
                }
                var nn=false;
                for (x=0;x<an.length;x++) {
                  if (tryit(an[x])) {
                    nn=true;
                  }
                }
                if (yy&&(!nn)) {
                  //console.log(fo,'yy:'+yy+' nn:'+nn+' rv:true');
                  return true;
                }
                //console.log(fo,'yy:'+yy+' nn:'+nn+' rv:false');
                return false;
                } //-_check_call
            // Z_DOT evt keypress(evt):bool      ::doc::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.63762738008402301.2015.11.16.23.15.26.736|+\
              ,keypress:function(evt){
                if (__.data.showkeys) {console.log('document.keypress',evt.keyCode);}
                var cas=__.utils.cas(evt);
                var foc=$(':focus');
                if (foc.length==1&&foc.prop('tagName')==='TEXTAREA') {return;}
                var code='k_'+cas+'p'+evt.keyCode;
                return this._do_call(code,evt);
                } //-keypress
            // Z_DOT evt keyup(evt):bool         ::doc::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.96077738008402301.2015.11.16.23.16.17.069|+\
              ,keyup:function(evt){
                if (__.data.showkeys) {console.log('document.keyup',evt.keyCode);}
                var foc=$(':focus');
                //console.log('document.keyup.tagName',foc.prop('tagName'));
                var cas=__.utils.cas(evt);
                if (this._keysdown['_'+evt.keyCode]!==undefined) {
                  delete this._keysdown['_'+evt.keyCode];
                }
                var code='k_'+cas+'u'+evt.keyCode;
                return this._do_call(code,evt);
                } //-keyup
            // Z_DOT evt keydown(evt):bool       ::doc::event::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.42611838008402301.2015.11.16.23.16.51.624|+\
              ,keydown:function(evt){
                if (__.data.showkeys) {console.log('document.keydown',evt.keyCode);}
                var foc=$(':focus');
                if (foc.length==1&&foc.prop('tagName')==='TEXTAREA') {return;}
                if (this._keysdown['_'+evt.keyCode]!==undefined) {return;}
                //if (document.sk) {console.log('document.keydown',evt.keyCode);}
                this._keysdown['_'+evt.keyCode]=new Date();
                var cas=__.utils.cas(evt);
                var code='k_'+cas+'d'+evt.keyCode;
                return this._do_call(code,evt);
                } //-keydown
            } //-doc
        } //-event
    // Z_DOT struct ear::__         ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.28294838008402301.2015.11.16.23.17.29.282|struct
      ,ear:{
        // Z_DOT var _name:string                                     ::ear::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.78678838008402301.2015.11.16.23.18.07.687|+@,
          _name:'ear'
        // Z_DOT evt _msg_listener_(request,sender,sendResponse):void ::ear::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.72612938008402301.2015.11.16.23.18.41.627|-\
          ,_msg_listener_:function(request, sender, sendResponse){
            } //-_msg_listener_
        // Z_DOT evt _evt_listener_(e):void                           ::ear::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.47218938008402301.2015.11.16.23.19.41.274|-\
          ,_evt_listener_:function(e){
            var data=e.detail;
            if (data==='idle') {__.inits.idle._init();}
            if (data==='k') {
              if (__.data.showkeys===true) {
                __.data.showkeys=false;
              }else{
                __.data.showkeys=true;
              }
            }
            if (data.indexOf('? ')==0) {
              var c=data.substr(2);
              console.log(eval(c));
            }
            //console.log("Mra received "+data);
            } //-_evt_listener
        // Z_DOT cmd _init_():void                                    ::ear::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.13653048008402301.2015.11.16.23.20.35.631|-!
          ,_init_:function(){
            document.addEventListener('MraEvent', this._evt_listener_);
            chrome.runtime.onMessage.addListener(this._msg_listener_);
            } //-_init_
        // Z_DOT cmd _init():void                                     ::ear::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.28546048008402301.2015.11.16.23.21.04.582|%!
          ,_init:function(){
            this._init_();
            } //_init
        } //-ear
    // Z_DOT struct init::__        ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.23577648008402301.2015.11.16.23.31.17.532|struct
      ,init:{
        // Z_DOT var _name:string ::init::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.72780748008402301.2015.11.16.23.31.48.727|+@,
          _name:'init'
        // Z_DOT cmd _init():void ::init::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.52807948008402301.2015.11.16.23.36.10.825|-!
          ,_init_:function(){
            //console.info('__.init');
            __.inits._init();
            } //-_init_
        // Z_DOT cmd _init():void ::init::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.87999948008402301.2015.11.16.23.36.39.978|%!
          ,_init:function(){
            this._init_();
            } //-_init
        } //-init
    // Z_DOT struct go::__          ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.93173058008402301.2015.11.16.23.37.17.139|struct
      ,go:{
        // Z_DOT var _name:string                       ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.22346058008402301.2015.11.16.23.37.44.322|+@,
          _name:'go'
        // Z_DOT var _tgt:object                        ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.72659058008402301.2015.11.16.23.38.15.627|-,
          ,_tgt:null
        // Z_DOT cmd _boot():void                       ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.09113158008402301.2015.11.16.23.38.51.190|-!
          ,_boot:function(){
            console.info('Podio BOOT');
            if (typeof jQuery==='undefined') {
              console.info('Waiting on jQuery...');
              setTimeout(function(){
                __._boot();
              },1000);
              return;
            }
            __.init._init();
            } //-boot
        // Z_DOT cmd _body_vis_():void                  ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.16495158008402301.2015.11.16.23.39.19.461|-!
          ,_body_vis_:function(){
            $('body').addClass('vis');
            } //-_body_vis
        // Z_DOT cmd _style_on_load_():void             ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.76818158008402301.2015.11.16.23.39.41.867|-!
          ,_style_on_load_:function(){
            console.log('graphite loaded');
            //setTimeout(__._body_vis_,500);
            __.go._body_vis_();
            //__._boot();
            } //-_style_on_load_
        // Z_DOT cmd _bootstrap_():void                 ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.83661258008402301.2015.11.16.23.40.16.638|-!
          ,_bootstrap_:function(){
            var style = document.createElement('link');
            style.rel = 'stylesheet';
            style.type = 'text/css';
            style.href = chrome.extension.getURL('styles.css');
            style.id='graphite';
            style.onload=this._style_on_load_;
            document.head.appendChild(style);
            this._boot();
            } //-bootstrap
        // Z_DOT cmd _each_mutation_(mutation):void     ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.79935258008402301.2015.11.16.23.40.53.997|-!
          ,_each_mutation_:function(mutation){
            if (mutation.target.nodeName == "BODY") {
              if (__.data.first_body_mutation===undefined) {
                __.go._bootstrap_();
                __.data.first_body_mutation=true;
                __.data.body_observer.disconnect();
                delete __.data.body_observer;
              }
            }
            } //-_each_mutation_
        // Z_DOT cmd _observe_for_body_(mutations):void ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.29067258008402301.2015.11.16.23.41.16.092|-!
          ,_observe_for_body_:function(mutations){
            mutations.forEach(__.go._each_mutation_);
            } //-_observe_for_body_
        // Z_DOT cmd _ignition_():void                  ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.96042358008402301.2015.11.16.23.42.04.069|-!
          ,_ignition_:function(){
            var first=true;
            __.data.body_observer = new MutationObserver(this._observe_for_body_);
            __.data.body_observer.observe(document, { childList: true, subtree: true });
            } //-ignition
        // Z_DOT cmd _launch_():void                    ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.58464358008402301.2015.11.16.23.42.26.485|-!
          ,_launch_:function(){
            this._ignition_();
            //$('html').css('background','#000');
            } //-_launch
        // Z_DOT cmd _prelaunch_():void                 ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.41707358008402301.2015.11.16.23.42.50.714|-!
          ,_prelaunch_:function(){
            this._launch_();
            } //-_prelaunch
        // Z_DOT cmd _init():void                       ::go::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.48219358008402301.2015.11.16.23.43.11.284|-!
          ,_init:function(that){
            this._tgt=that;
            this._prelaunch_();
            } //-_init
        } //go
    // Z_DOT cmd launch():void ::__ ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.25441458008402301.2015.11.16.23.43.34.452|+!
      ,launch:function(){
        __.go._init(this);
        } //-launch
    }; //-__
// Z_DOT cmd main():void ::script::pgs-1.0::pgs_vpw::_tool::!mark::C::script.js z.74887458008402301.2015.11.16.23.44.38.847|+!
  (function(){
    $('html').css('background','#000');
    __.launch();
    })();







