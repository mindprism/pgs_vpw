//chrome.tabs.create({url: "http://google.com", selected: false});


// When the extension is installed or upgraded ...
chrome.runtime.onInstalled.addListener(function() {
  // Replace all rules ...
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    // With a new rule ...
    chrome.declarativeContent.onPageChanged.addRules([
      {
        // That fires when a page's URL contains a 'g' ...
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { urlContains: '/podio.com/' },
          })
        ],
        // And shows the extension's page action.
        actions: [ new chrome.declarativeContent.ShowPageAction() ]
      }
    ]);
  });
});



chrome.tabs.onUpdated.addListener(checkForValidUrl);
function checkForValidUrl(tabId, changeInfo, tab) {
  if (tab.url !== undefined && changeInfo.status == "complete") {
    chrome.pageAction.setPopup({tabId: tabId,popup: "popup.html"});
    //chrome.pageAction.setTitle({tabId: tabId,title: resp.cashback});
  }
};


var tab_views={};


// chrome.pageAction.onClicked.addListener(function(tab){
//   //var myName = tab.url.split(".")[0].slice(7);
// });

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (0) {
      console.log(sender.tab ?
                   "from a content script:" + sender.tab.url :
                  "from the extension");
    }
    if (false) {
    }else  if (request.greeting == "set-options"){
      chrome.storage.sync.set(request.data);
    }else  if (request.greeting == "get-options"){
      chrome.storage.sync.get("podio_graphite", function(data){
        sendResponse(data);
        });
    }else  if (request.greeting == "log"){
      console.log(request.data);
    }else  if (request.greeting == "test"){
      // chrome.tabs.get(1,function(t){
      //   console.log(t.url);
      // });
      chrome.tabs.query({},function(tabs){
        tabs.forEach(function(tab,x){
          console.log('tab:',x,tab.url);
        });
      });
    }
  });

function broadcast(o){
  chrome.tabs.query({url:'https://podio.com/*'},function(tabs){
    tabs.forEach(function(tab,x){
      chrome.tabs.sendMessage(tab.id,o, function(response) {});
      chrome.tabs.sendMessage(tab.id,{action:'tab-id',tab_id:tab.id}, function(response) {});
    });
  });
}


function reload_podios(){
  chrome.tabs.query({url:'https://podio.com/*'},function(tabs){
    tabs.forEach(function(tab){
      chrome.tabs.reload(tab.id,{bypassCache:true});
    });
  });
}
reload_podios();
//console.log('Viz',Viz);
//console.log('Viz',window.Viz);
//var ss='digraph G { a -> b; }';
//console.log(Viz(ss,'svg'));
