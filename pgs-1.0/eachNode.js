/*jshint asi:true, laxcomma:true, eqeqeq:false, laxbreak:true, boss: true*/
var o={
  _name:'o'
  ,_int:1
  ,_bool:false
  ,_null:null
  ,_date:new Date()
  ,a:{
    a_int:1
    ,a_bool:false
    ,a_null:null
    ,a_date:new Date()
    ,a1:{
      a1_int:1
      ,a1_bool:false
      ,a1_null:null
      ,a1_date:new Date()
      ,_init:function(){}
    }
  }
  ,b:{
     _init:function(){}
  }
};

function test(){
    eachNode(o,function(name,item,type,object,parent,depth,path,indexes){
        //console.log('TEST:NAME:'+name+' depth:'+depth);
        console.log('TEST:PATH:'+path+' depth:'+depth+' NAME:'+name+' ITEM:'+item);
        if (name==='a1_null'){return 'finish';}
    //},['object','string','function','number','boolean','date']);
    },['function']);

    //alert(1);
}
test();

function eachNode(object,fn,wantTypes,re,_bag){

    if (_bag===undefined){
      console.log('eachNode:BEGIN=============================================');
    }
    if (_bag===undefined){
      var bag={
          index:-1
          ,didIndex:-1
          ,_parents:[]
          ,_paths:[]
          ,did:[]
          ,returns:{items:[],dupes:[]}
      };
      Object.defineProperty(bag,'depth',{
        get:function(){
          return this._parents.length;
        }
        ,set:function(){}
        ,enumerable:true
        ,configurable: false
      });
      Object.defineProperty(bag,'parent',{
        get:function(){
          return this._parents.length>0?this._parents[this._parents.length-1]:undefined;
        }
        ,set:function(v){this._parents.push(v);}
        ,enumerable:true
        ,configurable: false
        });
      Object.defineProperty(bag,'parents',{
        get:function(){var a=[];var x;for(x=0;x<this._parents.length;x++){a.push(this._parents[x]);}return a;}
        ,set:function(){}
        ,enumerable: true
        ,configurable: false
      });
      Object.defineProperty(bag,'path',{
        get:function(){var p=this._paths.join('.');
                       p=p===''?p:p+'.';
                        return p+''+this._name;}
        ,set:function(){}
        ,enumerable: true
        ,configurable: false
      });
      bag.push=function(){
        //console.log('push:this._name:'+this._name,this._paths);
        this._parents.push(this._object);
        this._paths.push(this._name);
        //console.log('push2',this._paths,'depth',this.depth);
        return eachNode(this._item,fn,wantTypes,re,this);
      };
      bag.pop=function(rv){
        //console.log('pop',this._paths);
        this._item=this._parents.pop();
        this._paths.pop();
        this._name=this._paths.length>0?this._paths[this._paths.length-1]:'';
        //console.log('pop2',this._paths,'depth',this.depth);
        if (rv===null){
          this._breaking=true;
        }
        if (this.depth===0) {
          return this;
        }
        return rv;
      };
      bag.path=function(){
        var s='';
        var x=0;
        for (x=0;x<this._parents.length;x++){s+='.'+this._parents[x]._name;}
        return s;
      }
      _bag=bag;
    }
    fn=fn!==undefined?fn:function(name,item,type,object,parent,depth,path,indexes){return true;};
    wantTypes=wantTypes?wantTypes:['object'];
    var itemIndex= -1, itemDidIndex=-1, name, item, rv,indexes, rvv;
    //console.log('doing object',object);
    _bag._object=object;
    for(name in object){
        if(!object.hasOwnProperty(name)){continue;}
        //console.log('eachNode:name:'+name);
        _bag._name=name;
        item=object[name];
        _bag._item=item;
        //console.log('eachNode:item:'+name);
        itemIndex++;
        _bag.index++;
        var type=typeof item;
        if (['function','object'].indexOf(type)!==-1&&item!==null){
            //console.log('_bag.did.indexOf(item):'+name);
            if (_bag.did.indexOf(item)!==-1){
                indexes={itemIndex:itemIndex,itemDidIndex:itemDidIndex,didIndex:_bag.didIndex,index:_bag.index};
                _bag.returns.dupes.push({item:item
                                         ,name:name
                                         ,object:object
                                         ,type:type
                                         ,parent:_bag.parent
                                         ,depth:_bag.depth
                                         ,path:_bag.path
                                         ,indexes:indexes
                                         });
                continue;
            }else{
                //console.log('eachNode:_bag.did.push(item):'+name);
                _bag.did.push(item);
            }
        }
        if(wantTypes.indexOf(type)!==-1){
            var doit=re===undefined?true:re.test(name);
            if (doit){
                itemDidIndex++;
                _bag.didIndex++;
                // undefined: no push, true: push, false: no push, -1: no push, 'done':abort level, 'finish': abort all
                indexes={itemIndex:itemIndex,itemDidIndex:itemDidIndex,didIndex:_bag.didIndex,index:_bag.index};
                rv=fn(name,item,type,object,_bag.parent,_bag.depth,_bag.path,indexes);
                //console.log('rv',rv);
                if (false){
                }else if (rv==='done'){
                    break;
                }else if (rv==='finish'){
                    return _bag.pop(null);
                }else if (rv){ // '==' is OK here
                    _bag.returns.items.push({name:name
                                             ,item:item
                                             ,object:object
                                             ,type:type
                                             ,parent:_bag.parent
                                             ,depth:_bag.depth
                                             ,path:_bag.path
                                             ,indexes:indexes});
                }else if (!rv||rv===-1||rv===undefined){  // '==' is OK here
                    // nada
                }
            }
        } // not want type
        if (type==='object'&&item!==null){
            //_bag.parent=object;
            //console.log('PUSHING:',item);
            rv=_bag.push();
            //rv=eachNode(item,fn,wantTypes,re,_bag);
            if(rv===null){
                return _bag.pop(rv);
                //break;
            }
        }
        if (_bag._breaking){
          break;
        }
    }
    //console.log('returning from object',object)
    return _bag.pop();

    //if (_bag.depth===0){
    //    return _bag;
    //}
    //_bag.depth--;
    //return;
}




